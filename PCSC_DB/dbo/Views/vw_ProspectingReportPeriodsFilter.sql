﻿CREATE VIEW [dbo].[vw_ProspectingReportPeriodsFilter]
AS
SELECT DISTINCT 
                      dbo.ProspectingReport.ReportingPeriod, dbo.ProspectingReport.Zone, dbo.ProspectingReport.District, dbo.ProspectingReport.DealerId, 
                      dbo.vw_ProspectingReportPeriods.sort, dbo.vw_ProspectingReportPeriods.ReportingYear
FROM         dbo.ProspectingReport INNER JOIN
                      dbo.vw_ProspectingReportPeriods ON dbo.ProspectingReport.ReportingPeriod = dbo.vw_ProspectingReportPeriods.ReportingPeriod
WHERE     (dbo.ProspectingReport.ProspectingType LIKE '%QTR%')

