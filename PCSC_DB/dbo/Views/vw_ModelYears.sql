﻿CREATE VIEW [dbo].[vw_ModelYears]
AS
SELECT DISTINCT TOP (100) PERCENT ModelYear, LEFT(ModelYear, 4) AS ModelYear4
FROM         dbo.MarketShareModelYr
WHERE     (ModelYear IS NOT NULL)
ORDER BY ModelYear4 DESC

