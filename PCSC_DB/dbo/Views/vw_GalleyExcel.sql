﻿CREATE VIEW [dbo].[vw_GalleyExcel]
AS
SELECT     dbo.GalleyListing.ReportRunDate, dbo.GalleyListing.Zone, dbo.GalleyListing.District, dbo.GalleyListing.DealerID, dbo.GalleyListing.OrderID, dbo.GalleyListing.PID, 
                      dbo.GalleyListing.CustomerFirstName, dbo.GalleyListing.CustomerLastName, dbo.GalleyListing.Address, dbo.GalleyListing.City, dbo.GalleyListing.State, 
                      dbo.GalleyListing.Zip, dbo.GalleyListing.VIN, dbo.GalleyListing.Model, dbo.GalleyListing.ModelYear, dbo.lkp_Segment.SegmentDescription AS Segment, 
                      dbo.lkp_Channel.ChannelDescription AS Channel, dbo.GalleyListing.OrderName, dbo.GalleyListing.OffLease, dbo.GalleyListing.SalesMsg, 
                      dbo.GalleyListing.CustomerPhone
FROM         dbo.GalleyListing INNER JOIN
                      dbo.lkp_Channel ON dbo.GalleyListing.Channel = dbo.lkp_Channel.Channel INNER JOIN
                      dbo.lkp_Segment ON dbo.GalleyListing.Segment = dbo.lkp_Segment.Segment

