﻿CREATE VIEW [dbo].[vw_CustomerUpdateExcel]
AS
SELECT     ReportRunDate, Zone, District, DealerID, CASE WHEN ChangeFlag = 'D' THEN 'Delete' ELSE 'Modified' END AS ChangeFlag, VIN, CustomerFirstName, 
                      CustomerLastName, Address, City, State, Zip
FROM         dbo.CustomerUpdate

