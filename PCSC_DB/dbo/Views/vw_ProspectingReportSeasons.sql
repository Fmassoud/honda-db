﻿CREATE VIEW [dbo].[vw_ProspectingReportSeasons]
AS
SELECT DISTINCT RIGHT(RTRIM(ReportingPeriod), 4) AS ReportingYear, LEFT(ReportingPeriod, 3) AS SeasonAbr
FROM         dbo.ProspectingReport

