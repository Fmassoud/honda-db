﻿CREATE VIEW [dbo].[vw_ProspectingTypes]
AS
SELECT DISTINCT dbo.lkp_ProspectingType.ProspectingDescription, dbo.ProspectingReport.ProspectingType, dbo.ProspectingReport.ReportingPeriod
FROM         dbo.lkp_ProspectingType INNER JOIN
                      dbo.ProspectingReport ON dbo.lkp_ProspectingType.ProspectingType = dbo.ProspectingReport.ProspectingType

