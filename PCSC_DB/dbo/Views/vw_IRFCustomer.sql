﻿CREATE VIEW [dbo].[vw_IRFCustomer]
AS
SELECT     dbo.IRFCustomer.*, dbo.lkp_Segment.SegmentDescription
FROM         dbo.IRFCustomer INNER JOIN
                      dbo.lkp_Segment ON dbo.IRFCustomer.Segment = dbo.lkp_Segment.Segment

