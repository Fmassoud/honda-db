﻿CREATE VIEW [dbo].[vw_ProspectingTypesFilter]
AS
SELECT DISTINCT 
                      dbo.lkp_ProspectingType.ProspectingDescription, dbo.ProspectingReport.ProspectingType, dbo.ProspectingReport.ReportingPeriod, 
                      dbo.ProspectingReport.Zone, dbo.ProspectingReport.District, dbo.ProspectingReport.DealerId
FROM         dbo.lkp_ProspectingType INNER JOIN
                      dbo.ProspectingReport ON dbo.lkp_ProspectingType.ProspectingType = dbo.ProspectingReport.ProspectingType

