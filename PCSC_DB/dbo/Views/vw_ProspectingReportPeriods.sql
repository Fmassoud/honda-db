﻿CREATE VIEW [dbo].[vw_ProspectingReportPeriods]
AS
SELECT     TOP (100) PERCENT dbo.lkp_Season.sort, dbo.lkp_Season.Season + ' ' + dbo.vw_ProspectingReportSeasons.ReportingYear AS ReportingPeriod, 
                      dbo.vw_ProspectingReportSeasons.ReportingYear
FROM         dbo.lkp_Season INNER JOIN
                      dbo.vw_ProspectingReportSeasons ON dbo.lkp_Season.SeasonAbr = dbo.vw_ProspectingReportSeasons.SeasonAbr
ORDER BY dbo.vw_ProspectingReportSeasons.ReportingYear, dbo.lkp_Season.sort

