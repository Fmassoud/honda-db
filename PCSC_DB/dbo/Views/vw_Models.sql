﻿CREATE VIEW [dbo].[vw_Models]
AS
SELECT DISTINCT TOP (100) PERCENT Model
FROM         dbo.ReminderReport
WHERE     (Model IS NOT NULL)
ORDER BY Model

