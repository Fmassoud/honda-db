﻿CREATE VIEW [dbo].[vw_RTMMarketShare]
AS
SELECT     CASE WHEN LEFT(ReportingMonth, 3) = 'Jan' THEN '20' + RIGHT(ReportingMonth, 2) + '01' WHEN LEFT(ReportingMonth, 3) 
                      = 'Feb' THEN '20' + RIGHT(ReportingMonth, 2) + '02' WHEN LEFT(ReportingMonth, 3) = 'Mar' THEN '20' + RIGHT(ReportingMonth, 2) 
                      + '03' WHEN LEFT(ReportingMonth, 3) = 'Apr' THEN '20' + RIGHT(ReportingMonth, 2) + '04' WHEN LEFT(ReportingMonth, 3) 
                      = 'May' THEN '20' + RIGHT(ReportingMonth, 2) + '05' WHEN LEFT(ReportingMonth, 3) = 'Jun' THEN '20' + RIGHT(ReportingMonth, 2) 
                      + '06' WHEN LEFT(ReportingMonth, 3) = 'Jul' THEN '20' + RIGHT(ReportingMonth, 2) + '07' WHEN LEFT(ReportingMonth, 3) 
                      = 'Aug' THEN '20' + RIGHT(ReportingMonth, 2) + '08' WHEN LEFT(ReportingMonth, 3) = 'Sep' THEN '20' + RIGHT(ReportingMonth, 2) 
                      + '09' WHEN LEFT(ReportingMonth, 3) = 'Oct' THEN '20' + RIGHT(ReportingMonth, 2) + '10' WHEN LEFT(ReportingMonth, 3) 
                      = 'Nov' THEN '20' + RIGHT(ReportingMonth, 2) + '11' WHEN LEFT(ReportingMonth, 3) = 'Dec' THEN '20' + RIGHT(ReportingMonth, 2) 
                      + '12' ELSE ReportingMonth END AS reportYearAndMonth, Zone AS salesZoneCode, District AS salesDistrictCode, DealerID AS dealerCode, 
                      MarketShare, LoyalCount, MarketShareCount
FROM         dbo.MarketShare

