﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDistrictCountByZone]
	-- Add the parameters for the stored procedure here
	@ZoneID varchar(10) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT COUNT(DISTINCT ServiceDistrict) As totalCount FROM Dealer WHERE ServiceZone = @ZoneID
END

