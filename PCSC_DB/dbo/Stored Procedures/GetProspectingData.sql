﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetProspectingData]
	-- Add the parameters for the stored procedure here
	
	@Where varchar(3000) = ''
AS
BEGIN
	DECLARE @Select varchar(1000)
	DECLARE @FullStatement nvarchar(3000)
	SET @Select = 'SELECT 
					SUM(QuantityMailed) As QuantityMailed, 
					SUM(Responses) AS Responses, 
					SUM(TotalNumROs) AS TotalNumROs, 
					SUM(ServiceRevenue) As ServiceRevenue, 
					SUM(MailingCost) AS MailingCost, 
					SUM(EmailsOpened) AS EmailsOpened 
					FROM ProspectingReport WHERE 1 = 1 '
	SET @FullStatement = @Select + @Where
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
      EXECUTE sp_executesql @FullStatement
	
END

