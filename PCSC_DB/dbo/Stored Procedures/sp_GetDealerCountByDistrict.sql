﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDealerCountByDistrict]
	-- Add the parameters for the stored procedure here
	@ZoneID varchar(10) = '',
	@DistrictID varchar(10) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF (@ZoneID > '') AND  (@DistrictID > '')
		SELECT COUNT(DISTINCT DealerID) As totalCount FROM Dealer WHERE ServiceZone = @ZoneID AND ServiceDistrict = @DistrictID
	ELSE IF (@ZoneID > '')
		SELECT COUNT(DISTINCT DealerID) As totalCount FROM Dealer WHERE ServiceZone = @ZoneID 
	ELSE
		SELECT COUNT(DISTINCT DealerID) As totalCount FROM Dealer 
	
END

