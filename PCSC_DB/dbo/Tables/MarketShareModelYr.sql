﻿CREATE TABLE [dbo].[MarketShareModelYr] (
    [ReportingMonth]   VARCHAR (5) NULL,
    [Zone]             VARCHAR (2) NULL,
    [District]         VARCHAR (1) NULL,
    [DealerID]         VARCHAR (6) NULL,
    [ModelYear]        VARCHAR (5) NULL,
    [Count]            INT         NULL,
    [LoyalCount]       INT         NULL,
    [MarketShareCount] INT         NULL,
    [MarketShare]      FLOAT (53)  NULL
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModelYr_8_284592848__K1_6_9]
    ON [dbo].[MarketShareModelYr]([ReportingMonth] ASC)
    INCLUDE([Count], [MarketShare]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModelYr_8_284592848__K5_K1_6_7_8]
    ON [dbo].[MarketShareModelYr]([ModelYear] ASC, [ReportingMonth] ASC)
    INCLUDE([Count], [LoyalCount], [MarketShareCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModelYr_8_284592848__K1]
    ON [dbo].[MarketShareModelYr]([ReportingMonth] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModelYr_8_284592848__K5]
    ON [dbo].[MarketShareModelYr]([ModelYear] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModelYr_8_284592848__K4_K5_K1_K3_K2_6_7_8]
    ON [dbo].[MarketShareModelYr]([DealerID] ASC, [ModelYear] ASC, [ReportingMonth] ASC, [District] ASC, [Zone] ASC)
    INCLUDE([LoyalCount], [Count], [MarketShareCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModelYr_8_284592848__K4_K3_K2_1_6_9]
    ON [dbo].[MarketShareModelYr]([DealerID] ASC, [District] ASC, [Zone] ASC)
    INCLUDE([ReportingMonth], [Count], [MarketShare]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModelYr_8_284592848__K5_K1_K3_K2_6_7_8]
    ON [dbo].[MarketShareModelYr]([ModelYear] ASC, [ReportingMonth] ASC, [District] ASC, [Zone] ASC)
    INCLUDE([Count], [LoyalCount], [MarketShareCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModelYr_8_284592848__K1_K5_K2_6_7_8]
    ON [dbo].[MarketShareModelYr]([ReportingMonth] ASC, [ModelYear] ASC, [Zone] ASC)
    INCLUDE([LoyalCount], [Count], [MarketShareCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModelYr_8_284592848__K1_K3_K2_6_9]
    ON [dbo].[MarketShareModelYr]([ReportingMonth] ASC, [District] ASC, [Zone] ASC)
    INCLUDE([Count], [MarketShare]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModelYr_8_284592848__K1_K2_6_9]
    ON [dbo].[MarketShareModelYr]([ReportingMonth] ASC, [Zone] ASC)
    INCLUDE([Count], [MarketShare]);


GO
CREATE STATISTICS [_dta_stat_284592848_5_1]
    ON [dbo].[MarketShareModelYr]([ModelYear], [ReportingMonth]);


GO
CREATE STATISTICS [_dta_stat_284592848_5_1_2]
    ON [dbo].[MarketShareModelYr]([ModelYear], [ReportingMonth], [Zone]);


GO
CREATE STATISTICS [_dta_stat_284592848_2_3_4]
    ON [dbo].[MarketShareModelYr]([Zone], [District], [DealerID]);


GO
CREATE STATISTICS [_dta_stat_284592848_1_5_3]
    ON [dbo].[MarketShareModelYr]([ReportingMonth], [ModelYear], [District]);


GO
CREATE STATISTICS [_dta_stat_284592848_4_1_5_3]
    ON [dbo].[MarketShareModelYr]([DealerID], [ReportingMonth], [ModelYear], [District]);


GO
CREATE STATISTICS [_dta_stat_284592848_5_2_3_4]
    ON [dbo].[MarketShareModelYr]([ModelYear], [Zone], [District], [DealerID]);


GO
CREATE STATISTICS [_dta_stat_284592848_2_3_5_1]
    ON [dbo].[MarketShareModelYr]([Zone], [District], [ModelYear], [ReportingMonth]);


GO
CREATE STATISTICS [_dta_stat_284592848_1_2_3_4_5]
    ON [dbo].[MarketShareModelYr]([ReportingMonth], [Zone], [District], [DealerID], [ModelYear]);

