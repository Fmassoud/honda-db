﻿CREATE TABLE [dbo].[IRFCustomer] (
    [DealerID]  INT          NULL,
    [Segment]   VARCHAR (2)  NULL,
    [Address]   VARCHAR (45) NULL,
    [City]      VARCHAR (20) NULL,
    [State]     VARCHAR (2)  NULL,
    [Zip]       VARCHAR (5)  NULL,
    [Latitude]  REAL         NULL,
    [Longitude] REAL         NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_IRFCustomer]
    ON [dbo].[IRFCustomer]([DealerID] ASC) WITH (FILLFACTOR = 90);

