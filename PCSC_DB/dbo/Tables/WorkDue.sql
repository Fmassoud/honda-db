﻿CREATE TABLE [dbo].[WorkDue] (
    [CommunicationDate] DATETIME      NULL,
    [ReportRunWeek]     VARCHAR (6)   NULL,
    [Zone]              VARCHAR (2)   NULL,
    [District]          VARCHAR (1)   NULL,
    [DealerID]          VARCHAR (6)   NULL,
    [LetterCode]        VARCHAR (4)   NULL,
    [CustomerName]      VARCHAR (50)  NULL,
    [Phone]             VARCHAR (15)  NULL,
    [VIN]               VARCHAR (20)  NULL,
    [ModelYear]         VARCHAR (15)  NULL,
    [ServiceDue]        VARCHAR (MAX) NULL,
    [ServiceAdvisor]    VARCHAR (20)  NULL,
    [LastMileage]       VARCHAR (20)  NULL,
    [LastVisit]         VARCHAR (20)  NULL,
    [FirstVisit]        VARCHAR (20)  NULL,
    [OffLease]          VARCHAR (2)   NULL,
    [SalesMsg]          VARCHAR (2)   NULL
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_WorkDue_8_1708597921__K5_K4_K6_K3_K2]
    ON [dbo].[WorkDue]([District] ASC, [Zone] ASC, [DealerID] ASC, [ReportRunWeek] ASC, [CommunicationDate] ASC);


GO
CREATE STATISTICS [_dta_stat_1708597921_4_5_6]
    ON [dbo].[WorkDue]([Zone], [District], [DealerID]);


GO
CREATE STATISTICS [_dta_stat_1708597921_3_2_6_5]
    ON [dbo].[WorkDue]([ReportRunWeek], [CommunicationDate], [DealerID], [District]);


GO
CREATE STATISTICS [_dta_stat_1708597921_3_6_5_4_2]
    ON [dbo].[WorkDue]([ReportRunWeek], [DealerID], [District], [Zone], [CommunicationDate]);


GO
CREATE STATISTICS [_dta_stat_1708597921_6_5]
    ON [dbo].[WorkDue]([DealerID], [District]);

