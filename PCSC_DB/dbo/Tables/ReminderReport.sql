﻿CREATE TABLE [dbo].[ReminderReport] (
    [ReportRunDate]  VARCHAR (5)  NULL,
    [ReportingMonth] VARCHAR (5)  NULL,
    [Zone]           VARCHAR (2)  NULL,
    [District]       VARCHAR (1)  NULL,
    [DealerID]       VARCHAR (6)  NULL,
    [LetterCode]     VARCHAR (4)  NULL,
    [Channel]        VARCHAR (2)  NULL,
    [Model]          VARCHAR (20) NULL,
    [QuantityMailed] INT          NULL,
    [Responses]      INT          NULL,
    [TotalNumROs]    INT          NULL,
    [ServiceRevenue] FLOAT (53)   NULL,
    [MailingCost]    FLOAT (53)   NULL,
    [ResponseRate]   FLOAT (53)   NULL,
    [AvgROAmt]       FLOAT (53)   NULL,
    [ROI]            FLOAT (53)   NULL,
    [EmailsOpened]   FLOAT (53)   NULL
);


GO
CREATE CLUSTERED INDEX [ix_ALL]
    ON [dbo].[ReminderReport]([ReportingMonth] ASC, [Zone] ASC, [District] ASC, [DealerID] ASC, [LetterCode] ASC, [Channel] ASC, [Model] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ReminderReport_8_396593247__K8]
    ON [dbo].[ReminderReport]([Model] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ReminderReport_8_396593247__K3_K4_K5_K2]
    ON [dbo].[ReminderReport]([Zone] ASC, [District] ASC, [DealerID] ASC, [ReportingMonth] ASC);


GO
CREATE STATISTICS [_dta_stat_396593247_3_4_5]
    ON [dbo].[ReminderReport]([Zone], [District], [DealerID]);

