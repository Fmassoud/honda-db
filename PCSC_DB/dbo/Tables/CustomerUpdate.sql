﻿CREATE TABLE [dbo].[CustomerUpdate] (
    [ReportRunDate]     SMALLDATETIME NULL,
    [Zone]              VARCHAR (2)   NULL,
    [District]          VARCHAR (1)   NULL,
    [DealerID]          VARCHAR (6)   NULL,
    [ChangeFlag]        VARCHAR (1)   NULL,
    [VIN]               VARCHAR (20)  NULL,
    [CustomerFirstName] VARCHAR (25)  NULL,
    [CustomerLastName]  VARCHAR (25)  NULL,
    [Address]           VARCHAR (50)  NULL,
    [City]              VARCHAR (40)  NULL,
    [State]             VARCHAR (2)   NULL,
    [Zip]               VARCHAR (5)   NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_CustomerUpdate]
    ON [dbo].[CustomerUpdate]([DealerID] ASC, [District] ASC, [Zone] ASC, [ReportRunDate] ASC) WITH (FILLFACTOR = 90);

