﻿CREATE TABLE [dbo].[MarketShareDistance] (
    [ReportingMonth]   VARCHAR (5)  NULL,
    [Zone]             VARCHAR (2)  NULL,
    [District]         VARCHAR (1)  NULL,
    [DealerID]         VARCHAR (6)  NULL,
    [Distance]         VARCHAR (20) NULL,
    [Count]            INT          NULL,
    [LoyalCount]       INT          NULL,
    [MarketShareCount] INT          NULL,
    [MarketShare]      FLOAT (53)   NULL
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareDistance_8_252592734__K1_6_9]
    ON [dbo].[MarketShareDistance]([ReportingMonth] ASC)
    INCLUDE([Count], [MarketShare]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareDistance_8_252592734__K1]
    ON [dbo].[MarketShareDistance]([ReportingMonth] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareDistance_8_252592734__K1_K2_K3_K4_K5_6_7_8]
    ON [dbo].[MarketShareDistance]([ReportingMonth] ASC, [Zone] ASC, [District] ASC, [DealerID] ASC, [Distance] ASC)
    INCLUDE([LoyalCount], [MarketShareCount], [Count]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareDistance_8_252592734__K1_K2_K3_K5_6_7_8]
    ON [dbo].[MarketShareDistance]([ReportingMonth] ASC, [Zone] ASC, [District] ASC, [Distance] ASC)
    INCLUDE([LoyalCount], [MarketShareCount], [Count]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareDistance_8_252592734__K1_K5_K2_6_7_8]
    ON [dbo].[MarketShareDistance]([ReportingMonth] ASC, [Distance] ASC, [Zone] ASC)
    INCLUDE([LoyalCount], [MarketShareCount], [Count]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareDistance_8_252592734__K2_K3_K4_1_6_9]
    ON [dbo].[MarketShareDistance]([Zone] ASC, [District] ASC, [DealerID] ASC)
    INCLUDE([Count], [MarketShare], [ReportingMonth]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareDistance_8_252592734__K2_K3_K1_6_9]
    ON [dbo].[MarketShareDistance]([Zone] ASC, [District] ASC, [ReportingMonth] ASC)
    INCLUDE([Count], [MarketShare]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareDistance_8_252592734__K2_K1_6_9]
    ON [dbo].[MarketShareDistance]([Zone] ASC, [ReportingMonth] ASC)
    INCLUDE([Count], [MarketShare]);


GO
CREATE STATISTICS [_dta_stat_252592734_5_1]
    ON [dbo].[MarketShareDistance]([Distance], [ReportingMonth]);


GO
CREATE STATISTICS [_dta_stat_252592734_3_1_5]
    ON [dbo].[MarketShareDistance]([District], [ReportingMonth], [Distance]);


GO
CREATE STATISTICS [_dta_stat_252592734_5_2_1]
    ON [dbo].[MarketShareDistance]([Distance], [Zone], [ReportingMonth]);


GO
CREATE STATISTICS [_dta_stat_252592734_2_3_5_1]
    ON [dbo].[MarketShareDistance]([Zone], [District], [Distance], [ReportingMonth]);


GO
CREATE STATISTICS [_dta_stat_252592734_4_3_1_5]
    ON [dbo].[MarketShareDistance]([DealerID], [District], [ReportingMonth], [Distance]);


GO
CREATE STATISTICS [_dta_stat_252592734_5_2_3_4_1]
    ON [dbo].[MarketShareDistance]([Distance], [Zone], [District], [DealerID], [ReportingMonth]);

