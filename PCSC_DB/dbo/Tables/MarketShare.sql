﻿CREATE TABLE [dbo].[MarketShare] (
    [ReportingMonth]          VARCHAR (5) NULL,
    [Zone]                    VARCHAR (2) NULL,
    [District]                VARCHAR (1) NULL,
    [DealerID]                VARCHAR (6) NULL,
    [Count]                   INT         NULL,
    [DefaultCount]            INT         NULL,
    [LoyalCount]              INT         NULL,
    [MarketShareCount]        INT         NULL,
    [DefaultLoyalCount]       INT         NULL,
    [DefaultMarketShareCount] INT         NULL,
    [MarketShare]             FLOAT (53)  NULL,
    [DefaultMarketShare]      FLOAT (53)  NULL
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShare_8_1564597408__K1_K2_K3_K4_5_7_8_9_10_11]
    ON [dbo].[MarketShare]([ReportingMonth] ASC, [Zone] ASC, [District] ASC, [DealerID] ASC)
    INCLUDE([Count], [LoyalCount], [MarketShareCount], [DefaultLoyalCount], [MarketShare], [DefaultMarketShareCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShare_8_1564597408__K1_5_7_8_9_10]
    ON [dbo].[MarketShare]([ReportingMonth] ASC)
    INCLUDE([DefaultLoyalCount], [MarketShareCount], [LoyalCount], [Count], [DefaultMarketShareCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShare_8_1564597408__K1_5_11]
    ON [dbo].[MarketShare]([ReportingMonth] ASC)
    INCLUDE([Count], [MarketShare]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShare_8_1564597408__K1]
    ON [dbo].[MarketShare]([ReportingMonth] ASC);


GO
CREATE STATISTICS [_dta_stat_1564597408_2_3_4_1]
    ON [dbo].[MarketShare]([Zone], [District], [DealerID], [ReportingMonth]);

