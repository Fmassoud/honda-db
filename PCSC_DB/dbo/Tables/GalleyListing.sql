﻿CREATE TABLE [dbo].[GalleyListing] (
    [ReportRunDate]     VARCHAR (10)  NULL,
    [Zone]              VARCHAR (2)   NULL,
    [District]          VARCHAR (1)   NULL,
    [DealerID]          VARCHAR (6)   NULL,
    [OrderID]           VARCHAR (8)   NULL,
    [PID]               VARCHAR (9)   NULL,
    [CustomerFirstName] VARCHAR (20)  NULL,
    [CustomerLastName]  VARCHAR (20)  NULL,
    [Address]           VARCHAR (45)  NULL,
    [City]              VARCHAR (20)  NULL,
    [State]             VARCHAR (2)   NULL,
    [Zip]               VARCHAR (5)   NULL,
    [VIN]               VARCHAR (20)  NULL,
    [Model]             VARCHAR (20)  NULL,
    [ModelYear]         VARCHAR (50)  NULL,
    [Segment]           VARCHAR (10)  NULL,
    [Channel]           VARCHAR (50)  NULL,
    [OrderName]         VARCHAR (MAX) NULL,
    [OffLease]          VARCHAR (2)   NULL,
    [SalesMsg]          VARCHAR (2)   NULL,
    [CustomerPhone]     VARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_GalleyListing_8_1788598206__K4_K1_K3_K2_K5_18]
    ON [dbo].[GalleyListing]([DealerID] ASC, [ReportRunDate] ASC, [District] ASC, [Zone] ASC, [OrderID] ASC)
    INCLUDE([OrderName]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_GalleyListing_8_1788598206__K1]
    ON [dbo].[GalleyListing]([ReportRunDate] ASC);


GO
CREATE STATISTICS [_dta_stat_1788598206_3_1]
    ON [dbo].[GalleyListing]([District], [ReportRunDate]);


GO
CREATE STATISTICS [_dta_stat_1788598206_4_3_1]
    ON [dbo].[GalleyListing]([DealerID], [District], [ReportRunDate]);


GO
CREATE STATISTICS [_dta_stat_1788598206_5_4_3_1]
    ON [dbo].[GalleyListing]([OrderID], [DealerID], [District], [ReportRunDate]);


GO
CREATE STATISTICS [_dta_stat_1788598206_1_2_3_4_5]
    ON [dbo].[GalleyListing]([ReportRunDate], [Zone], [District], [DealerID], [OrderID]);

