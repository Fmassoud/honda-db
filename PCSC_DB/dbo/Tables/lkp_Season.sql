﻿CREATE TABLE [dbo].[lkp_Season] (
    [SeasonID]  INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Season]    VARCHAR (50) NULL,
    [sort]      INT          NULL,
    [SeasonAbr] NCHAR (10)   NULL,
    CONSTRAINT [PK_lkp_Season] PRIMARY KEY CLUSTERED ([SeasonID] ASC)
);

