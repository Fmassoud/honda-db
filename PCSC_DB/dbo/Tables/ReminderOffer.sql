﻿CREATE TABLE [dbo].[ReminderOffer] (
    [ReportingMonth]     VARCHAR (5)   NULL,
    [Zone]               VARCHAR (2)   NULL,
    [District]           VARCHAR (1)   NULL,
    [DealerID]           VARCHAR (6)   NULL,
    [LetterCode]         VARCHAR (4)   NULL,
    [Model]              VARCHAR (20)  NULL,
    [AdLines1]           VARCHAR (100) NULL,
    [AdLines2]           VARCHAR (100) NULL,
    [AdLines3]           VARCHAR (100) NULL,
    [LeftCouponCode]     VARCHAR (20)  NULL,
    [LeftCouponHeading]  VARCHAR (100) NULL,
    [LeftCouponValue]    VARCHAR (50)  NULL,
    [RightCouponCode]    VARCHAR (20)  NULL,
    [RightCouponHeading] VARCHAR (100) NULL,
    [RightCouponValue]   VARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ReminderOffer]
    ON [dbo].[ReminderOffer]([ReportingMonth] ASC) WITH (FILLFACTOR = 90);

