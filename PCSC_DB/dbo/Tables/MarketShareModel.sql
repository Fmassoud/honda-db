﻿CREATE TABLE [dbo].[MarketShareModel] (
    [ReportingMonth]   VARCHAR (5)  NULL,
    [Zone]             VARCHAR (2)  NULL,
    [District]         VARCHAR (1)  NULL,
    [DealerID]         VARCHAR (6)  NULL,
    [Model]            VARCHAR (20) NULL,
    [Count]            INT          NULL,
    [LoyalCount]       INT          NULL,
    [MarketShareCount] INT          NULL,
    [MarketShare]      FLOAT (53)   NULL
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModel_8_268592791__K1_6_9]
    ON [dbo].[MarketShareModel]([ReportingMonth] ASC)
    INCLUDE([Count], [MarketShare]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModel_8_268592791__K5_K1_6_7_8]
    ON [dbo].[MarketShareModel]([Model] ASC, [ReportingMonth] ASC)
    INCLUDE([Count], [LoyalCount], [MarketShareCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MarketShareModel_8_268592791__K1]
    ON [dbo].[MarketShareModel]([ReportingMonth] ASC);


GO
CREATE STATISTICS [_dta_stat_268592791_5_1]
    ON [dbo].[MarketShareModel]([Model], [ReportingMonth]);

