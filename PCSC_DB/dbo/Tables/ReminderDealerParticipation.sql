﻿CREATE TABLE [dbo].[ReminderDealerParticipation] (
    [ReportingMonth]               VARCHAR (5) NULL,
    [DealerID]                     VARCHAR (6) NULL,
    [LetterCode]                   VARCHAR (4) NULL,
    [ParticipationFlag]            VARCHAR (1) NULL,
    [DistrictParticipationPercent] FLOAT (53)  NULL,
    [ZoneParticipation]            FLOAT (53)  NULL,
    [NationalParticipation]        FLOAT (53)  NULL
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ReminderDealerParticipation_8_364593133__K3_K4_K2_7]
    ON [dbo].[ReminderDealerParticipation]([LetterCode] ASC, [ParticipationFlag] ASC, [DealerID] ASC)
    INCLUDE([NationalParticipation]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ReminderDealerParticipation_8_364593133__K1]
    ON [dbo].[ReminderDealerParticipation]([ReportingMonth] ASC);


GO
CREATE STATISTICS [_dta_stat_364593133_1]
    ON [dbo].[ReminderDealerParticipation]([ReportingMonth]);


GO
CREATE STATISTICS [_dta_stat_364593133_3_4]
    ON [dbo].[ReminderDealerParticipation]([LetterCode], [ParticipationFlag]);


GO
CREATE STATISTICS [_dta_stat_364593133_2_3_4]
    ON [dbo].[ReminderDealerParticipation]([DealerID], [LetterCode], [ParticipationFlag]);


GO
CREATE STATISTICS [_dta_stat_364593133_3_2_1]
    ON [dbo].[ReminderDealerParticipation]([LetterCode], [DealerID], [ReportingMonth]);

