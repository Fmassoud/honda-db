﻿CREATE TABLE [dbo].[lkp_LetterCode] (
    [LetterCode]        VARCHAR (4)   NOT NULL,
    [LetterDescription] VARCHAR (100) NULL,
    [sort]              INT           NULL,
    [active]            BIT           NULL,
    CONSTRAINT [PK_lkp_LetterCode] PRIMARY KEY CLUSTERED ([LetterCode] ASC)
);

