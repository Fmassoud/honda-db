﻿CREATE TABLE [dbo].[ASC_DataTest] (
    [ReportRunDate]         SMALLDATETIME NULL,
    [ReportingMonth]        SMALLDATETIME NULL,
    [Zone]                  VARCHAR (2)   NULL,
    [District]              VARCHAR (1)   NULL,
    [DealerID]              VARCHAR (6)   NULL,
    [Segment]               VARCHAR (2)   NULL,
    [ASC_Code]              VARCHAR (2)   NULL,
    [ASC_Created_Appts]     INT           NULL,
    [ASC_Completed_Appts]   INT           NULL,
    [ASC_RO]                INT           NULL,
    [ASC_Service_Revenue]   FLOAT (53)    NULL,
    [Month_Total_RO]        INT           NULL,
    [Total_Service_Revenue] FLOAT (53)    NULL
);

