﻿CREATE TABLE [dbo].[Dealer] (
    [DealerID]        VARCHAR (6)  NOT NULL,
    [ServiceZone]     VARCHAR (2)  NULL,
    [ServiceDistrict] VARCHAR (1)  NULL,
    [DealerName]      VARCHAR (40) NULL,
    [DealerAddress]   VARCHAR (30) NULL,
    [City]            VARCHAR (30) NULL,
    [State]           VARCHAR (2)  NULL,
    [Zip]             VARCHAR (5)  NULL,
    [Phone]           VARCHAR (15) NULL,
    [Active]          VARCHAR (1)  NULL,
    [NewDlrID]        VARCHAR (6)  NULL,
    [Latitude]        FLOAT (53)   NULL,
    [Longitude]       FLOAT (53)   NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Dealer]
    ON [dbo].[Dealer]([DealerID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Dealer_1]
    ON [dbo].[Dealer]([ServiceZone] ASC, [ServiceDistrict] ASC, [DealerID] ASC) WITH (FILLFACTOR = 90);

