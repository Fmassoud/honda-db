﻿CREATE TABLE [dbo].[lkp_ASC_Type] (
    [ASC_Code]        VARCHAR (2)  NOT NULL,
    [ASC_Description] VARCHAR (20) NULL,
    CONSTRAINT [PK_ASC_Appt_Type] PRIMARY KEY CLUSTERED ([ASC_Code] ASC)
);

