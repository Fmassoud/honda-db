﻿CREATE TABLE [dbo].[NonServicer] (
    [ReportingMonth] VARCHAR (5)  NULL,
    [ReportRunDate]  VARCHAR (5)  NULL,
    [Zone]           VARCHAR (2)  NULL,
    [District]       VARCHAR (1)  NULL,
    [DealerID]       VARCHAR (6)  NULL,
    [CustomerName]   VARCHAR (50) NULL,
    [Phone]          VARCHAR (15) NULL,
    [VIN]            VARCHAR (20) NULL,
    [ModelYear]      VARCHAR (20) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_NonServicer]
    ON [dbo].[NonServicer]([ReportingMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_NonServicer_8_1628597636__K3_K4_K5_K1]
    ON [dbo].[NonServicer]([Zone] ASC, [District] ASC, [DealerID] ASC, [ReportingMonth] ASC);


GO
CREATE STATISTICS [_dta_stat_1628597636_1_5_4_3]
    ON [dbo].[NonServicer]([ReportingMonth], [DealerID], [District], [Zone]);

