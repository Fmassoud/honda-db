﻿CREATE TABLE [dbo].[ProspectingReport] (
    [ReportRunDate]   VARCHAR (5)   NULL,
    [ReportingPeriod] VARCHAR (12)  NULL,
    [ProspectingType] VARCHAR (3)   NULL,
    [ResponseDays]    INT           NULL,
    [Zone]            VARCHAR (2)   NULL,
    [District]        VARCHAR (1)   NULL,
    [DealerId]        VARCHAR (6)   NULL,
    [OrderId]         VARCHAR (8)   NULL,
    [OrderName]       VARCHAR (100) NULL,
    [Channel]         VARCHAR (2)   NULL,
    [Segment]         VARCHAR (2)   NULL,
    [QuantityMailed]  INT           NULL,
    [Responses]       INT           NULL,
    [TotalNumROs]     INT           NULL,
    [ServiceRevenue]  FLOAT (53)    NULL,
    [MailingCost]     FLOAT (53)    NULL,
    [ResponseRate]    FLOAT (53)    NULL,
    [AvgROAmount]     FLOAT (53)    NULL,
    [ROI]             FLOAT (53)    NULL,
    [EmailsOpened]    INT           NULL,
    [MailDate]        SMALLDATETIME NULL
);


GO
CREATE STATISTICS [_dta_stat_153116382_21_3]
    ON [dbo].[ProspectingReport]([MailDate], [ProspectingType]);


GO
CREATE STATISTICS [_dta_stat_153116382_4_3]
    ON [dbo].[ProspectingReport]([ResponseDays], [ProspectingType]);


GO
CREATE STATISTICS [_dta_stat_153116382_2_21_3]
    ON [dbo].[ProspectingReport]([ReportingPeriod], [MailDate], [ProspectingType]);


GO
CREATE STATISTICS [_dta_stat_153116382_2_3_4]
    ON [dbo].[ProspectingReport]([ReportingPeriod], [ProspectingType], [ResponseDays]);


GO
CREATE STATISTICS [_dta_stat_153116382_5_6]
    ON [dbo].[ProspectingReport]([Zone], [District]);


GO
CREATE STATISTICS [_dta_stat_153116382_6_21]
    ON [dbo].[ProspectingReport]([District], [MailDate]);


GO
CREATE STATISTICS [_dta_stat_153116382_6_3_21]
    ON [dbo].[ProspectingReport]([District], [ProspectingType], [MailDate]);


GO
CREATE STATISTICS [_dta_stat_153116382_3_7_6]
    ON [dbo].[ProspectingReport]([ProspectingType], [DealerId], [District]);


GO
CREATE STATISTICS [_dta_stat_153116382_5_3_2]
    ON [dbo].[ProspectingReport]([Zone], [ProspectingType], [ReportingPeriod]);


GO
CREATE STATISTICS [_dta_stat_153116382_3_5_6]
    ON [dbo].[ProspectingReport]([ProspectingType], [Zone], [District]);


GO
CREATE STATISTICS [_dta_stat_153116382_3_21_5_6]
    ON [dbo].[ProspectingReport]([ProspectingType], [MailDate], [Zone], [District]);


GO
CREATE STATISTICS [_dta_stat_153116382_21_3_7_6]
    ON [dbo].[ProspectingReport]([MailDate], [ProspectingType], [DealerId], [District]);


GO
CREATE STATISTICS [_dta_stat_153116382_3_2_5_6]
    ON [dbo].[ProspectingReport]([ProspectingType], [ReportingPeriod], [Zone], [District]);


GO
CREATE STATISTICS [_dta_stat_153116382_21_3_2_5]
    ON [dbo].[ProspectingReport]([MailDate], [ProspectingType], [ReportingPeriod], [Zone]);


GO
CREATE STATISTICS [_dta_stat_153116382_6_2_3_21]
    ON [dbo].[ProspectingReport]([District], [ReportingPeriod], [ProspectingType], [MailDate]);


GO
CREATE STATISTICS [_dta_stat_153116382_4_7_2_6_3]
    ON [dbo].[ProspectingReport]([ResponseDays], [DealerId], [ReportingPeriod], [District], [ProspectingType]);


GO
CREATE STATISTICS [_dta_stat_153116382_21_7_6_2_5]
    ON [dbo].[ProspectingReport]([MailDate], [DealerId], [District], [ReportingPeriod], [Zone]);


GO
CREATE STATISTICS [_dta_stat_153116382_5_21_6_7_3]
    ON [dbo].[ProspectingReport]([Zone], [MailDate], [District], [DealerId], [ProspectingType]);


GO
CREATE STATISTICS [_dta_stat_153116382_2_5_6_7_3]
    ON [dbo].[ProspectingReport]([ReportingPeriod], [Zone], [District], [DealerId], [ProspectingType]);


GO
CREATE STATISTICS [_dta_stat_153116382_7_6_5_3_2_21]
    ON [dbo].[ProspectingReport]([DealerId], [District], [Zone], [ProspectingType], [ReportingPeriod], [MailDate]);


GO
CREATE STATISTICS [_dta_stat_153116382_4_2_3_5_6_7]
    ON [dbo].[ProspectingReport]([ResponseDays], [ReportingPeriod], [ProspectingType], [Zone], [District], [DealerId]);


GO
CREATE STATISTICS [_dta_stat_153116382_21_2_3_4_5_6_7]
    ON [dbo].[ProspectingReport]([MailDate], [ReportingPeriod], [ProspectingType], [ResponseDays], [Zone], [District], [DealerId]);


GO
CREATE STATISTICS [_dta_stat_153116382_11_4_3]
    ON [dbo].[ProspectingReport]([Segment], [ResponseDays], [ProspectingType]);


GO
CREATE STATISTICS [_dta_stat_153116382_21_11_2_3]
    ON [dbo].[ProspectingReport]([MailDate], [Segment], [ReportingPeriod], [ProspectingType]);


GO
CREATE STATISTICS [_dta_stat_153116382_11_2_3_4_21]
    ON [dbo].[ProspectingReport]([Segment], [ReportingPeriod], [ProspectingType], [ResponseDays], [MailDate]);


GO
CREATE STATISTICS [_dta_stat_153116382_5_7]
    ON [dbo].[ProspectingReport]([Zone], [DealerId]);


GO
CREATE STATISTICS [_dta_stat_153116382_5_3_2_7]
    ON [dbo].[ProspectingReport]([Zone], [ProspectingType], [ReportingPeriod], [DealerId]);


GO
CREATE STATISTICS [_dta_stat_153116382_7_3_5]
    ON [dbo].[ProspectingReport]([DealerId], [ProspectingType], [Zone]);

