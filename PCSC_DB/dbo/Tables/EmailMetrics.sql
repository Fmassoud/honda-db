﻿CREATE TABLE [dbo].[EmailMetrics] (
    [RecipientID]       VARCHAR (50)   NULL,
    [RecipientType]     VARCHAR (50)   NULL,
    [MailingID]         VARCHAR (30)   NULL,
    [ReportID]          VARCHAR (30)   NULL,
    [CampaignID]        VARCHAR (30)   NULL,
    [Email]             VARCHAR (50)   NULL,
    [EventType]         VARCHAR (50)   NULL,
    [EventTimestamp]    DATETIME       NULL,
    [BodyType]          VARCHAR (20)   NULL,
    [ContentID]         VARCHAR (20)   NULL,
    [ClickName]         VARCHAR (8000) NULL,
    [URL]               VARCHAR (8000) NULL,
    [ConversionAction]  VARCHAR (50)   NULL,
    [ConversionDetail]  VARCHAR (50)   NULL,
    [ConversionAmount]  VARCHAR (50)   NULL,
    [SuppressionReason] VARCHAR (50)   NULL,
    [CustomerID]        VARCHAR (20)   NULL,
    [Address]           VARCHAR (1000) NULL,
    [ORDERID]           VARCHAR (50)   NULL,
    [ZONE]              VARCHAR (10)   NULL,
    [DISTRICT]          VARCHAR (10)   NULL,
    [DEALER_ID]         VARCHAR (10)   NULL,
    [PROG_ID]           VARCHAR (50)   NULL,
    [CAMPAIGNCODE]      VARCHAR (50)   NULL,
    [MAIL_DATE]         VARCHAR (20)   NULL,
    [LETTER_CODE]       VARCHAR (50)   NULL
);


GO
CREATE CLUSTERED INDEX [_dta_index_EmailMetrics_c_8_521117693__K19_K24_K7]
    ON [dbo].[EmailMetrics]([ORDERID] ASC, [CAMPAIGNCODE] ASC, [EventType] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_EmailMetrics_8_521117693__K19_K24_K7_K22_K1]
    ON [dbo].[EmailMetrics]([ORDERID] ASC, [CAMPAIGNCODE] ASC, [EventType] ASC, [DEALER_ID] ASC, [RecipientID] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_EmailMetrics_8_521117693__K24_K7_K22]
    ON [dbo].[EmailMetrics]([CAMPAIGNCODE] ASC, [EventType] ASC, [DEALER_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_EmailMetrics_8_521117693__K7_K19_K24_K8_K22_K1]
    ON [dbo].[EmailMetrics]([EventType] ASC, [ORDERID] ASC, [CAMPAIGNCODE] ASC, [EventTimestamp] ASC, [DEALER_ID] ASC, [RecipientID] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_EmailMetrics_8_521117693__K24_K7_K8_K22_K19]
    ON [dbo].[EmailMetrics]([CAMPAIGNCODE] ASC, [EventType] ASC, [EventTimestamp] ASC, [DEALER_ID] ASC, [ORDERID] ASC);


GO
CREATE STATISTICS [_dta_stat_521117693_19_24]
    ON [dbo].[EmailMetrics]([ORDERID], [CAMPAIGNCODE]);


GO
CREATE STATISTICS [_dta_stat_521117693_7_19_24]
    ON [dbo].[EmailMetrics]([EventType], [ORDERID], [CAMPAIGNCODE]);


GO
CREATE STATISTICS [_dta_stat_521117693_22_7_19]
    ON [dbo].[EmailMetrics]([DEALER_ID], [EventType], [ORDERID]);


GO
CREATE STATISTICS [_dta_stat_521117693_22_19_24]
    ON [dbo].[EmailMetrics]([DEALER_ID], [ORDERID], [CAMPAIGNCODE]);


GO
CREATE STATISTICS [_dta_stat_521117693_1_19_7_24]
    ON [dbo].[EmailMetrics]([RecipientID], [ORDERID], [EventType], [CAMPAIGNCODE]);


GO
CREATE STATISTICS [_dta_stat_521117693_24_22_7_19_1]
    ON [dbo].[EmailMetrics]([CAMPAIGNCODE], [DEALER_ID], [EventType], [ORDERID], [RecipientID]);


GO
CREATE STATISTICS [_dta_stat_521117693_8_24_7_22_19]
    ON [dbo].[EmailMetrics]([EventTimestamp], [CAMPAIGNCODE], [EventType], [DEALER_ID], [ORDERID]);


GO
CREATE STATISTICS [_dta_stat_521117693_7_8]
    ON [dbo].[EmailMetrics]([EventType], [EventTimestamp]);


GO
CREATE STATISTICS [_dta_stat_521117693_19_8_7]
    ON [dbo].[EmailMetrics]([ORDERID], [EventTimestamp], [EventType]);


GO
CREATE STATISTICS [_dta_stat_521117693_19_8_24]
    ON [dbo].[EmailMetrics]([ORDERID], [EventTimestamp], [CAMPAIGNCODE]);


GO
CREATE STATISTICS [_dta_stat_521117693_8_22_24]
    ON [dbo].[EmailMetrics]([EventTimestamp], [DEALER_ID], [CAMPAIGNCODE]);


GO
CREATE STATISTICS [_dta_stat_521117693_7_22_8]
    ON [dbo].[EmailMetrics]([EventType], [DEALER_ID], [EventTimestamp]);


GO
CREATE STATISTICS [_dta_stat_521117693_8_22_19_24]
    ON [dbo].[EmailMetrics]([EventTimestamp], [DEALER_ID], [ORDERID], [CAMPAIGNCODE]);


GO
CREATE STATISTICS [_dta_stat_521117693_19_24_7_8]
    ON [dbo].[EmailMetrics]([ORDERID], [CAMPAIGNCODE], [EventType], [EventTimestamp]);


GO
CREATE STATISTICS [_dta_stat_521117693_1_19_24_7_8]
    ON [dbo].[EmailMetrics]([RecipientID], [ORDERID], [CAMPAIGNCODE], [EventType], [EventTimestamp]);


GO
CREATE STATISTICS [_dta_stat_521117693_24_22_19_7_8_1]
    ON [dbo].[EmailMetrics]([CAMPAIGNCODE], [DEALER_ID], [ORDERID], [EventType], [EventTimestamp], [RecipientID]);

