﻿CREATE TABLE [dbo].[ClientSnapShot] (
    [ReportingMonth] VARCHAR (5) NULL,
    [Zone]           VARCHAR (2) NULL,
    [District]       VARCHAR (1) NULL,
    [DealerID]       VARCHAR (6) NULL,
    [Channel]        VARCHAR (2) NULL,
    [Segment]        VARCHAR (2) NULL,
    [Count]          INT         NULL,
    [DefaultCount]   INT         NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ClientSnapShot]
    ON [dbo].[ClientSnapShot]([ReportingMonth] ASC, [Zone] ASC, [District] ASC, [DealerID] ASC, [Channel] ASC, [Segment] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ClientSnapShot_8_217116610__K1_K6_7_8]
    ON [dbo].[ClientSnapShot]([ReportingMonth] ASC, [Segment] ASC)
    INCLUDE([Count], [DefaultCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ClientSnapShot_8_217116610__K1_7_8]
    ON [dbo].[ClientSnapShot]([ReportingMonth] ASC)
    INCLUDE([Count], [DefaultCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ClientSnapShot_8_217116610__K1_K6_7]
    ON [dbo].[ClientSnapShot]([ReportingMonth] ASC, [Segment] ASC)
    INCLUDE([Count]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ClientSnapShot_8_217116610__K1_K5_7]
    ON [dbo].[ClientSnapShot]([ReportingMonth] ASC, [Channel] ASC)
    INCLUDE([Count]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ClientSnapShot_8_217116610__K1_7]
    ON [dbo].[ClientSnapShot]([ReportingMonth] ASC)
    INCLUDE([Count]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ClientSnapShot_8_217116610__K1]
    ON [dbo].[ClientSnapShot]([ReportingMonth] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ClientSnapShot_8_217116610__K2_K3_K4_1_6_7_8]
    ON [dbo].[ClientSnapShot]([Zone] ASC, [District] ASC, [DealerID] ASC)
    INCLUDE([ReportingMonth], [Count], [Segment], [DefaultCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ClientSnapShot_8_217116610__K2_K1_K3_7]
    ON [dbo].[ClientSnapShot]([Zone] ASC, [ReportingMonth] ASC, [District] ASC)
    INCLUDE([Count]);


GO
CREATE STATISTICS [_dta_stat_217116610_6_1]
    ON [dbo].[ClientSnapShot]([Segment], [ReportingMonth]);


GO
CREATE STATISTICS [_dta_stat_217116610_1_2_6]
    ON [dbo].[ClientSnapShot]([ReportingMonth], [Zone], [Segment]);


GO
CREATE STATISTICS [_dta_stat_217116610_2_1_5]
    ON [dbo].[ClientSnapShot]([Zone], [ReportingMonth], [Channel]);


GO
CREATE STATISTICS [_dta_stat_217116610_2_1_3_5]
    ON [dbo].[ClientSnapShot]([Zone], [ReportingMonth], [District], [Channel]);


GO
CREATE STATISTICS [_dta_stat_217116610_1_2_3_6]
    ON [dbo].[ClientSnapShot]([ReportingMonth], [Zone], [District], [Segment]);


GO
CREATE STATISTICS [_dta_stat_217116610_2_1_3_4_6]
    ON [dbo].[ClientSnapShot]([Zone], [ReportingMonth], [District], [DealerID], [Segment]);

