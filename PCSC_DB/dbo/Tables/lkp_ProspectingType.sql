﻿CREATE TABLE [dbo].[lkp_ProspectingType] (
    [ProspectingType]        VARCHAR (3)   NOT NULL,
    [ProspectingDescription] VARCHAR (300) NULL,
    [sort]                   INT           NULL,
    CONSTRAINT [PK_lkp_ProspectingType] PRIMARY KEY CLUSTERED ([ProspectingType] ASC)
);

