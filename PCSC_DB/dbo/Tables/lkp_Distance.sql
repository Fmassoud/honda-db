﻿CREATE TABLE [dbo].[lkp_Distance] (
    [Distance]   VARCHAR (50) NULL,
    [sort]       INT          NULL,
    [DistanceID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [label]      VARCHAR (50) NULL,
    CONSTRAINT [PK_lkp_Distance] PRIMARY KEY CLUSTERED ([DistanceID] ASC)
);

