﻿CREATE TABLE [dbo].[Session] (
    [SessionID]        VARCHAR (300) NOT NULL,
    [IPAddress]        VARCHAR (20)  NOT NULL,
    [DealerID]         VARCHAR (10)  NULL,
    [ActingAsDealerID] VARCHAR (10)  NULL,
    [Tab]              VARCHAR (100) NULL,
    [DateTimeStamp]    DATETIME      NOT NULL,
    CONSTRAINT [PK_Session_1] PRIMARY KEY CLUSTERED ([SessionID] ASC)
);

