﻿CREATE TABLE [dbo].[lkp_Channel] (
    [Channel]            VARCHAR (2)  NOT NULL,
    [ChannelDescription] VARCHAR (50) NULL,
    [sort]               INT          NULL,
    CONSTRAINT [PK_lkp_Channel] PRIMARY KEY CLUSTERED ([Channel] ASC)
);

