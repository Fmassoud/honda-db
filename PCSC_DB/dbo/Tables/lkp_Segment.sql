﻿CREATE TABLE [dbo].[lkp_Segment] (
    [Segment]            VARCHAR (2)   NOT NULL,
    [SegmentDescription] VARCHAR (50)  NULL,
    [tooltip]            VARCHAR (800) NULL,
    CONSTRAINT [PK_lkp_Segment] PRIMARY KEY CLUSTERED ([Segment] ASC)
);

