﻿CREATE TABLE [dbo].[Users] (
    [FirstName]  NVARCHAR (50)  NOT NULL,
    [LastName]   NVARCHAR (50)  NOT NULL,
    [EmployeeID] NVARCHAR (50)  NOT NULL,
    [Zone]       VARCHAR (10)   NULL,
    [District]   VARCHAR (10)   NULL,
    [Title]      VARCHAR (300)  NULL,
    [ProductID]  VARCHAR (3)    NOT NULL,
    [Email]      VARCHAR (300)  NULL,
    [Notes]      VARCHAR (1000) NULL,
    [RecordID]   INT            IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([EmployeeID] ASC) WITH (FILLFACTOR = 90)
);

