﻿CREATE TABLE [dbo].[Regimens] (
    [ID]        INT          NOT NULL,
    [Label]     VARCHAR (50) NULL,
    [SortOrder] INT          NULL,
    [ProductID] INT          CONSTRAINT [DF_Regimens_ProductType] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Regimens] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

