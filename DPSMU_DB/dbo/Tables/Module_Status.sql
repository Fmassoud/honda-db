﻿CREATE TABLE [dbo].[Module_Status] (
    [ID]     INT          NOT NULL,
    [Status] VARCHAR (50) NULL,
    CONSTRAINT [PK_Module_Status] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

