﻿CREATE TABLE [dbo].[Product_Type] (
    [ProductID]   INT           IDENTITY (1, 1) NOT NULL,
    [Product]     NVARCHAR (2)  NULL,
    [Description] NVARCHAR (50) NULL
);

