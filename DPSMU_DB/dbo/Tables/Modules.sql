﻿CREATE TABLE [dbo].[Modules] (
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [Regimen]     INT            NULL,
    [Title]       VARCHAR (1000) NULL,
    [Link]        VARCHAR (100)  NULL,
    [Status]      INT            NULL,
    [Description] TEXT           NULL,
    [SortOrder]   INT            NULL,
    [ModuleType]  INT            NULL,
    [ProductID]   INT            NULL,
    CONSTRAINT [PK_Modules] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

