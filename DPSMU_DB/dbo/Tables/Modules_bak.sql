﻿CREATE TABLE [dbo].[Modules_bak] (
    [ID]          INT           NOT NULL,
    [Regimen]     INT           NULL,
    [Title]       VARCHAR (100) NULL,
    [Link]        VARCHAR (100) NULL,
    [Status]      INT           NULL,
    [Description] TEXT          NULL,
    [SortOrder]   INT           NULL,
    [ModuleType]  INT           NULL,
    [ProductID]   INT           NULL
);

