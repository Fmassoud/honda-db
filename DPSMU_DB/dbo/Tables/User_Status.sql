﻿CREATE TABLE [dbo].[User_Status] (
    [RecordID]      INT            IDENTITY (1, 1) NOT NULL,
    [RecordCreated] DATETIME       CONSTRAINT [DF_User_Status_RecordCreated] DEFAULT (getdate()) NULL,
    [EmployeeID]    VARCHAR (10)   NOT NULL,
    [ModuleID]      INT            NOT NULL,
    [ModuleStatus]  INT            NULL,
    [DateComplete]  DATETIME       NULL,
    [Bookmark]      VARCHAR (1000) NULL,
    CONSTRAINT [PK_User_Status] PRIMARY KEY CLUSTERED ([EmployeeID] ASC, [ModuleID] ASC) WITH (FILLFACTOR = 90)
);

