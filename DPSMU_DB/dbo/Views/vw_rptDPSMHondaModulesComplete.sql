﻿
CREATE VIEW [dbo].[vw_rptDPSMHondaModulesComplete]
AS
SELECT DISTINCT 
                      vw_rptDPSMHonda.FirstName, vw_rptDPSMHonda.LastName, vw_rptDPSMHonda.EmployeeID, 
                      vw_rptDPSMHonda.Zone, vw_rptDPSMHonda.District, vw_rptModulesHonda.Title, 
                      User_Status.ModuleID, User_Status.ModuleStatus, User_Status.DateComplete
FROM         User_Status INNER JOIN
                      vw_rptModulesHonda ON User_Status.ModuleID = vw_rptModulesHonda.ID INNER JOIN
                      vw_rptDPSMHonda ON User_Status.EmployeeID = vw_rptDPSMHonda.EmployeeID
WHERE     (User_Status.ModuleStatus = 3) AND (NOT (User_Status.DateComplete IS NULL))


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User_Status"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 184
               Right = 657
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vw_rptModulesHonda"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 208
               Right = 399
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vw_rptDPSMHonda"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 216
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_rptDPSMHondaModulesComplete';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_rptDPSMHondaModulesComplete';

