﻿create view qryDistinctMajorMetro as
SELECT DISTINCT tblMajorMetros.majorMetroID, tblMajorMetros.majorMetroName, Max(qryDistinctZips.longitude) AS FirstOflongitude, Max(qryDistinctZips.latitude) AS FirstOflatitude
FROM qryDistinctZips INNER JOIN tblMajorMetros ON qryDistinctZips.zip = tblMajorMetros.zip
GROUP BY tblMajorMetros.majorMetroID, tblMajorMetros.majorMetroName;
