﻿create view [Find duplicates for tblZip] as
SELECT (tblZip.zip) AS [zip Field], (tblZip.latitude) AS [latitude Field], (tblZip.longitude) AS [longitude Field], Count(tblZip.zip) AS NumberOfDups
FROM tblZip
GROUP BY tblZip.zip, tblZip.latitude, tblZip.longitude
HAVING (((Count(tblZip.zip))>1) AND ((Count(tblZip.longitude))>1));