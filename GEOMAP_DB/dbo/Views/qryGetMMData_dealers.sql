﻿/*ORDER BY qryDistinctMajorMetro.majorMetroID, tblData.prodDivision;*/
CREATE VIEW dbo.qryGetMMData_dealers
AS
SELECT     dbo.qryDistinctMajorMetro.majorMetroID, dbo.qryDistinctMajorMetro.majorMetroName, dbo.tblData.moduleID, dbo.tblModule.moduleName, 
                      dbo.qryDistinctMajorMetro.FirstOflongitude AS lon, dbo.qryDistinctMajorMetro.FirstOflatitude AS lat, dbo.tblData.prodDivision, dbo.tblData.ahmDivision, 
                      dbo.tblData.dptsID, dbo.qryDealer2MajorMetro.state, MAX(dbo.tblData.jobCode) AS FirstOfjobCode, dbo.tblDealers.dealerID, dbo.tblDealers.dealerName, 
                      (CASE WHEN (Len([midname]) > 0) THEN [firstname] + ' ' + [midname] + ' ' + [lastname] ELSE ([firstname] + ' ' + [lastname]) END) AS empname, 
                      MAX(dbo.tblJobCode.jobDescription) AS FirstOfjobDescription
FROM         dbo.qryDistinctMajorMetro INNER JOIN
                      dbo.tblData INNER JOIN
                      dbo.qryDealer2MajorMetro ON dbo.tblData.dealerID = dbo.qryDealer2MajorMetro.dealerID ON 
                      dbo.qryDistinctMajorMetro.majorMetroID = dbo.qryDealer2MajorMetro.majorMetroID INNER JOIN
                      dbo.tblDealers ON dbo.tblData.dealerID = dbo.tblDealers.dealerID INNER JOIN
                      dbo.tblModule ON dbo.tblData.moduleID = dbo.tblModule.moduleID INNER JOIN
                      dbo.tblJobCode ON dbo.tblData.jobCode = dbo.tblJobCode.jobCode
GROUP BY dbo.qryDistinctMajorMetro.majorMetroID, dbo.qryDistinctMajorMetro.majorMetroName, dbo.tblData.moduleID, dbo.tblModule.moduleName, 
                      dbo.qryDistinctMajorMetro.FirstOflongitude, dbo.qryDistinctMajorMetro.FirstOflatitude, dbo.tblData.prodDivision, dbo.tblData.ahmDivision, dbo.tblData.dptsID, 
                      dbo.qryDealer2MajorMetro.state, dbo.tblDealers.dealerID, dbo.tblDealers.dealerName, CASE WHEN (Len([midname]) > 0) 
                      THEN [firstname] + ' ' + [midname] + ' ' + [lastname] ELSE ([firstname] + ' ' + [lastname]) END

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "qryDistinctMajorMetro"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblData"
            Begin Extent = 
               Top = 6
               Left = 254
               Bottom = 114
               Right = 421
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "qryDealer2MajorMetro"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblDealers"
            Begin Extent = 
               Top = 114
               Left = 254
               Bottom = 222
               Right = 421
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblModule"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 205
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblJobCode"
            Begin Extent = 
               Top = 222
               Left = 243
               Bottom = 330
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'qryGetMMData_dealers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'qryGetMMData_dealers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'qryGetMMData_dealers';

