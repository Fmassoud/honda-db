﻿
CREATE VIEW [dbo].[qryGetDistricts_data]
AS 
   /*Generated by SQL Server Migration Assistant for Access version 5.2.1257.*/
   SELECT DISTINCT TOP 9223372036854775807 WITH TIES 
      [tblDealers].[prodDivision], 
      [tblDealers].[serviceZone], 
      [tblDealers].[serviceDistrict], 
      convert(varchar(10),[servicezone]) + convert(varchar(10),[servicedistrict]) AS ZD,
      [tblProdZoneDist].[lat], 
      [tblProdZoneDist].[lon], 
      [tblDealers].[dealerID], 
      [tblDealers].[dealerName], 
      [tblDealers].[address], 
      [tblDealers].[city], 
      [tblDealers].[state], 
      [tblDealers].[zip5], 
      [tblDealers].[zip4], 
      [tblDealers].[AssnPC]
   FROM 
      [tblProdZoneDist] 
         INNER JOIN [tblDealers] 
         ON 
            ([tblProdZoneDist].[dist] = [tblDealers].[serviceDistrict]) AND 
            ([tblProdZoneDist].[zone] = [tblDealers].[serviceZone]) AND 
            ([tblProdZoneDist].[prod] = [tblDealers].[prodDivision])
   ORDER BY convert(varchar(10),[servicezone]) + convert(varchar(10),[servicedistrict]) 

GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[qryGetDistricts_data]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'qryGetDistricts_data';

