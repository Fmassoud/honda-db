﻿/*
*   SSMA warning messages:
*   A2SS0030: View 'tblZip Query' has a name that might cause problems for the Access application to function correctly against SQL Server.
*/

CREATE VIEW dbo.[tblZip Query]
AS 
   /*Generated by SQL Server Migration Assistant for Access version 5.2.1257.*/
   SELECT [tblZip].[zip]
   FROM [tblZip]
GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblZip Query]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'tblZip Query';

