﻿CREATE TABLE [dbo].[tblJobCode] (
    [jobCode]        INT            DEFAULT ((0)) NOT NULL,
    [jobDescription] NVARCHAR (50)  NULL,
    [ahmDivision]    NVARCHAR (255) NOT NULL,
    [prodDivision]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [tblJobCode$PrimaryKey] PRIMARY KEY CLUSTERED ([jobCode] ASC, [ahmDivision] ASC, [prodDivision] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [tblJobCode$jobCode]
    ON [dbo].[tblJobCode]([jobCode] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblJobCode].[jobCode]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblJobCode', @level2type = N'INDEX', @level2name = N'tblJobCode$jobCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblJobCode]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblJobCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblJobCode].[jobCode]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblJobCode', @level2type = N'COLUMN', @level2name = N'jobCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblJobCode].[jobDescription]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblJobCode', @level2type = N'COLUMN', @level2name = N'jobDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblJobCode].[ahmDivision]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblJobCode', @level2type = N'COLUMN', @level2name = N'ahmDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblJobCode].[prodDivision]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblJobCode', @level2type = N'COLUMN', @level2name = N'prodDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblJobCode].[PrimaryKey]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblJobCode', @level2type = N'CONSTRAINT', @level2name = N'tblJobCode$PrimaryKey';

