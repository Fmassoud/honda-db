﻿CREATE TABLE [dbo].[tblZip] (
    [zip]            NVARCHAR (5) NULL,
    [latitude]       FLOAT (53)   NULL,
    [longitude]      FLOAT (53)   NULL,
    [SSMA_TimeStamp] ROWVERSION   NOT NULL,
    CONSTRAINT [SSMA_CC$tblZip$zip$disallow_zero_length] CHECK (len([zip])>(0))
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblZip]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblZip';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblZip].[zip]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblZip', @level2type = N'COLUMN', @level2name = N'zip';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblZip].[latitude]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblZip', @level2type = N'COLUMN', @level2name = N'latitude';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblZip].[longitude]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblZip', @level2type = N'COLUMN', @level2name = N'longitude';

