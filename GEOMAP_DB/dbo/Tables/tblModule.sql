﻿CREATE TABLE [dbo].[tblModule] (
    [moduleID]     NVARCHAR (50) NOT NULL,
    [moduleName]   NVARCHAR (50) NULL,
    [prodDivision] NVARCHAR (1)  NOT NULL,
    [sortOrder]    INT           NULL,
    CONSTRAINT [PK_tblModule] PRIMARY KEY CLUSTERED ([moduleID] ASC, [prodDivision] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [tblModule$moduleID]
    ON [dbo].[tblModule]([moduleID] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblModule].[moduleID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblModule', @level2type = N'INDEX', @level2name = N'tblModule$moduleID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblModule]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblModule';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblModule].[moduleID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblModule', @level2type = N'COLUMN', @level2name = N'moduleID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblModule].[moduleName]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblModule', @level2type = N'COLUMN', @level2name = N'moduleName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblModule].[sortOrder]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblModule', @level2type = N'COLUMN', @level2name = N'sortOrder';

