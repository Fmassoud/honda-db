﻿CREATE TABLE [dbo].[tblServiceDistrict] (
    [serviceDistrictID] INT          IDENTITY (1, 1) NOT NULL,
    [serviceDistrict]   NVARCHAR (1) NULL,
    CONSTRAINT [tblServiceDistrict$PrimaryKey] PRIMARY KEY CLUSTERED ([serviceDistrictID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [SSMA_CC$tblServiceDistrict$serviceDistrict$disallow_zero_length] CHECK (len([serviceDistrict])>(0))
);


GO
CREATE NONCLUSTERED INDEX [tblServiceDistrict$serviceDistrictID]
    ON [dbo].[tblServiceDistrict]([serviceDistrictID] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblServiceDistrict].[serviceDistrictID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblServiceDistrict', @level2type = N'INDEX', @level2name = N'tblServiceDistrict$serviceDistrictID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblServiceDistrict]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblServiceDistrict';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblServiceDistrict].[serviceDistrictID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblServiceDistrict', @level2type = N'COLUMN', @level2name = N'serviceDistrictID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblServiceDistrict].[serviceDistrict]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblServiceDistrict', @level2type = N'COLUMN', @level2name = N'serviceDistrict';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblServiceDistrict].[PrimaryKey]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblServiceDistrict', @level2type = N'CONSTRAINT', @level2name = N'tblServiceDistrict$PrimaryKey';

