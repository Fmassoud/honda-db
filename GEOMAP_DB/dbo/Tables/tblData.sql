﻿CREATE TABLE [dbo].[tblData] (
    [ahmDivision]  NVARCHAR (1)   NULL,
    [prodDivision] NVARCHAR (1)   NULL,
    [dealerID]     INT            CONSTRAINT [DF__tblData__dealerI__08EA5793] DEFAULT ((0)) NULL,
    [dptsID]       NVARCHAR (20)  NULL,
    [jobCode]      INT            CONSTRAINT [DF__tblData__jobCode__09DE7BCC] DEFAULT ((0)) NULL,
    [moduleID]     NVARCHAR (5)   NULL,
    [moduleName]   NVARCHAR (35)  NULL,
    [lastName]     NVARCHAR (150) NULL,
    [firstName]    NVARCHAR (150) NULL,
    [midName]      NVARCHAR (1)   NULL,
    [email]        NVARCHAR (150) NULL,
    CONSTRAINT [SSMA_CC$tblData$ahmDivision$disallow_zero_length] CHECK (len([ahmDivision])>(0)),
    CONSTRAINT [SSMA_CC$tblData$dptsID$disallow_zero_length] CHECK (len([dptsID])>(0)),
    CONSTRAINT [SSMA_CC$tblData$firstName$disallow_zero_length] CHECK (len([firstName])>(0)),
    CONSTRAINT [SSMA_CC$tblData$lastName$disallow_zero_length] CHECK (len([lastName])>(0)),
    CONSTRAINT [SSMA_CC$tblData$moduleID$disallow_zero_length] CHECK (len([moduleID])>(0)),
    CONSTRAINT [SSMA_CC$tblData$prodDivision$disallow_zero_length] CHECK (len([prodDivision])>(0))
);


GO
CREATE NONCLUSTERED INDEX [tblData$dptsID]
    ON [dbo].[tblData]([dptsID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [tblData$jobCode]
    ON [dbo].[tblData]([jobCode] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [tblData$moduleID]
    ON [dbo].[tblData]([moduleID] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[dptsID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'INDEX', @level2name = N'tblData$dptsID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[jobCode]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'INDEX', @level2name = N'tblData$jobCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[moduleID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'INDEX', @level2name = N'tblData$moduleID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[ahmDivision]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'COLUMN', @level2name = N'ahmDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[prodDivision]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'COLUMN', @level2name = N'prodDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[dealerID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'COLUMN', @level2name = N'dealerID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[dptsID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'COLUMN', @level2name = N'dptsID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[jobCode]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'COLUMN', @level2name = N'jobCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[moduleID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'COLUMN', @level2name = N'moduleID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[lastName]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'COLUMN', @level2name = N'lastName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[firstName]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'COLUMN', @level2name = N'firstName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[midName]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'COLUMN', @level2name = N'midName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblData].[email]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblData', @level2type = N'COLUMN', @level2name = N'email';

