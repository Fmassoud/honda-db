﻿CREATE TABLE [dbo].[tblProdDivision] (
    [prodDivisionID]   INT           IDENTITY (1, 1) NOT NULL,
    [prodDivision]     NVARCHAR (50) NULL,
    [prodDivisionName] NVARCHAR (50) NULL,
    CONSTRAINT [tblProdDivision$PrimaryKey] PRIMARY KEY CLUSTERED ([prodDivisionID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [SSMA_CC$tblProdDivision$prodDivision$disallow_zero_length] CHECK (len([prodDivision])>(0)),
    CONSTRAINT [SSMA_CC$tblProdDivision$prodDivisionName$disallow_zero_length] CHECK (len([prodDivisionName])>(0))
);


GO
CREATE NONCLUSTERED INDEX [tblProdDivision$AHMDivID]
    ON [dbo].[tblProdDivision]([prodDivisionID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [tblProdDivision$AHMDivisionCode]
    ON [dbo].[tblProdDivision]([prodDivision] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdDivision].[AHMDivID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdDivision', @level2type = N'INDEX', @level2name = N'tblProdDivision$AHMDivID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdDivision].[AHMDivisionCode]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdDivision', @level2type = N'INDEX', @level2name = N'tblProdDivision$AHMDivisionCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdDivision]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdDivision].[prodDivisionID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdDivision', @level2type = N'COLUMN', @level2name = N'prodDivisionID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdDivision].[prodDivision]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdDivision', @level2type = N'COLUMN', @level2name = N'prodDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdDivision].[prodDivisionName]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdDivision', @level2type = N'COLUMN', @level2name = N'prodDivisionName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdDivision].[PrimaryKey]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdDivision', @level2type = N'CONSTRAINT', @level2name = N'tblProdDivision$PrimaryKey';

