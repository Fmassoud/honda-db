﻿CREATE TABLE [dbo].[latlon] (
    [AvgOflongitude]  FLOAT (53)    NULL,
    [AvgOflatitude]   FLOAT (53)    NULL,
    [prodDivision]    NVARCHAR (50) NULL,
    [serviceZone]     INT           NULL,
    [serviceDistrict] NVARCHAR (50) NULL,
    [SSMA_TimeStamp]  ROWVERSION    NOT NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[latlon]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'latlon';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[latlon].[AvgOflongitude]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'latlon', @level2type = N'COLUMN', @level2name = N'AvgOflongitude';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[latlon].[AvgOflatitude]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'latlon', @level2type = N'COLUMN', @level2name = N'AvgOflatitude';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[latlon].[prodDivision]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'latlon', @level2type = N'COLUMN', @level2name = N'prodDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[latlon].[serviceZone]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'latlon', @level2type = N'COLUMN', @level2name = N'serviceZone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[latlon].[serviceDistrict]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'latlon', @level2type = N'COLUMN', @level2name = N'serviceDistrict';

