﻿CREATE TABLE [dbo].[tblProdZoneDist] (
    [ProdZoneDistCode] NVARCHAR (50) NOT NULL,
    [lat]              NVARCHAR (50) NULL,
    [lon]              NVARCHAR (50) NULL,
    [prod]             NVARCHAR (50) NULL,
    [zone]             INT           DEFAULT ((0)) NULL,
    [dist]             NVARCHAR (50) NULL,
    [zip]              NVARCHAR (50) NULL,
    CONSTRAINT [tblProdZoneDist$PrimaryKey] PRIMARY KEY CLUSTERED ([ProdZoneDistCode] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [tblProdZoneDist$ProdZoneDistCode]
    ON [dbo].[tblProdZoneDist]([ProdZoneDistCode] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdZoneDist].[ProdZoneDistCode]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdZoneDist', @level2type = N'INDEX', @level2name = N'tblProdZoneDist$ProdZoneDistCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdZoneDist]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdZoneDist';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdZoneDist].[ProdZoneDistCode]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdZoneDist', @level2type = N'COLUMN', @level2name = N'ProdZoneDistCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdZoneDist].[lat]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdZoneDist', @level2type = N'COLUMN', @level2name = N'lat';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdZoneDist].[lon]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdZoneDist', @level2type = N'COLUMN', @level2name = N'lon';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdZoneDist].[prod]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdZoneDist', @level2type = N'COLUMN', @level2name = N'prod';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdZoneDist].[zone]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdZoneDist', @level2type = N'COLUMN', @level2name = N'zone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdZoneDist].[dist]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdZoneDist', @level2type = N'COLUMN', @level2name = N'dist';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdZoneDist].[zip]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdZoneDist', @level2type = N'COLUMN', @level2name = N'zip';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblProdZoneDist].[PrimaryKey]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblProdZoneDist', @level2type = N'CONSTRAINT', @level2name = N'tblProdZoneDist$PrimaryKey';

