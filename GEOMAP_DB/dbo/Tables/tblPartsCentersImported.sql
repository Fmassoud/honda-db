﻿CREATE TABLE [dbo].[tblPartsCentersImported] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [AssnPC]         NVARCHAR (50)  NULL,
    [DSP]            NVARCHAR (255) NULL,
    [DlrNo]          INT            DEFAULT ((0)) NULL,
    [Field4]         NVARCHAR (255) NULL,
    [PrevNo]         NVARCHAR (255) NULL,
    [Field6]         NVARCHAR (255) NULL,
    [NewPC]          NVARCHAR (255) NULL,
    [Dlr Name]       NVARCHAR (255) NULL,
    [Dlr Addr]       NVARCHAR (255) NULL,
    [Dlr City]       NVARCHAR (255) NULL,
    [ST]             NVARCHAR (255) NULL,
    [Zip]            NVARCHAR (50)  NULL,
    [Area]           FLOAT (53)     NULL,
    [Pref]           FLOAT (53)     NULL,
    [Suff]           FLOAT (53)     NULL,
    [SSMA_TimeStamp] ROWVERSION     NOT NULL,
    CONSTRAINT [tblPartsCentersImported$PrimaryKey] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[ID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[AssnPC]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'AssnPC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[DSP]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'DSP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[DlrNo]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'DlrNo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[Field4]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'Field4';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[PrevNo]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'PrevNo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[Field6]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'Field6';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[NewPC]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'NewPC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[Dlr Name]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'Dlr Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[Dlr Addr]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'Dlr Addr';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[Dlr City]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'Dlr City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[ST]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'ST';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[Zip]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'Zip';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[Area]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'Area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[Pref]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'Pref';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[Suff]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'COLUMN', @level2name = N'Suff';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblPartsCentersImported].[PrimaryKey]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblPartsCentersImported', @level2type = N'CONSTRAINT', @level2name = N'tblPartsCentersImported$PrimaryKey';

