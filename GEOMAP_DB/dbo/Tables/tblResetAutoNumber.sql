﻿CREATE TABLE [dbo].[tblResetAutoNumber] (
    [ID] INT DEFAULT ((0)) NULL
);


GO
CREATE NONCLUSTERED INDEX [tblResetAutoNumber$ID]
    ON [dbo].[tblResetAutoNumber]([ID] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblResetAutoNumber].[ID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblResetAutoNumber', @level2type = N'INDEX', @level2name = N'tblResetAutoNumber$ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblResetAutoNumber]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblResetAutoNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblResetAutoNumber].[ID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblResetAutoNumber', @level2type = N'COLUMN', @level2name = N'ID';

