﻿CREATE TABLE [dbo].[tblMarket] (
    [marketID] INT           IDENTITY (1, 1) NOT NULL,
    [market]   NVARCHAR (50) NULL,
    CONSTRAINT [tblMarket$PrimaryKey] PRIMARY KEY CLUSTERED ([marketID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [SSMA_CC$tblMarket$market$disallow_zero_length] CHECK (len([market])>(0))
);


GO
CREATE NONCLUSTERED INDEX [tblMarket$marketID]
    ON [dbo].[tblMarket]([marketID] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMarket].[marketID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMarket', @level2type = N'INDEX', @level2name = N'tblMarket$marketID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMarket]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMarket';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMarket].[marketID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMarket', @level2type = N'COLUMN', @level2name = N'marketID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMarket].[market]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMarket', @level2type = N'COLUMN', @level2name = N'market';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMarket].[PrimaryKey]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMarket', @level2type = N'CONSTRAINT', @level2name = N'tblMarket$PrimaryKey';

