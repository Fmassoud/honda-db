﻿CREATE TABLE [dbo].[tblAHMDivision] (
    [AHMDivisionID]   INT           IDENTITY (1, 1) NOT NULL,
    [AHMDivision]     NVARCHAR (50) NULL,
    [AHMDivisionName] NVARCHAR (50) NULL,
    CONSTRAINT [tblAHMDivision$PrimaryKey] PRIMARY KEY CLUSTERED ([AHMDivisionID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [SSMA_CC$tblAHMDivision$AHMDivision$disallow_zero_length] CHECK (len([AHMDivision])>(0)),
    CONSTRAINT [SSMA_CC$tblAHMDivision$AHMDivisionName$disallow_zero_length] CHECK (len([AHMDivisionName])>(0))
);


GO
CREATE NONCLUSTERED INDEX [tblAHMDivision$AHMDivID]
    ON [dbo].[tblAHMDivision]([AHMDivisionID] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblAHMDivision].[AHMDivID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblAHMDivision', @level2type = N'INDEX', @level2name = N'tblAHMDivision$AHMDivID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblAHMDivision]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblAHMDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblAHMDivision].[AHMDivisionID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblAHMDivision', @level2type = N'COLUMN', @level2name = N'AHMDivisionID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblAHMDivision].[AHMDivision]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblAHMDivision', @level2type = N'COLUMN', @level2name = N'AHMDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblAHMDivision].[AHMDivisionName]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblAHMDivision', @level2type = N'COLUMN', @level2name = N'AHMDivisionName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblAHMDivision].[PrimaryKey]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblAHMDivision', @level2type = N'CONSTRAINT', @level2name = N'tblAHMDivision$PrimaryKey';

