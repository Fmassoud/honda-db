﻿CREATE TABLE [dbo].[tblModule_temp] (
    [moduleID]     NVARCHAR (50) NOT NULL,
    [moduleName]   NVARCHAR (50) NULL,
    [prodDivision] NVARCHAR (1)  NOT NULL,
    [sortOrder]    INT           NULL
);

