﻿CREATE TABLE [dbo].[tblServiceZone] (
    [serviceZoneID] INT      IDENTITY (1, 1) NOT NULL,
    [serviceZone]   SMALLINT DEFAULT ((0)) NULL,
    CONSTRAINT [tblServiceZone$PrimaryKey] PRIMARY KEY CLUSTERED ([serviceZoneID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [tblServiceZone$serviceZoneID]
    ON [dbo].[tblServiceZone]([serviceZoneID] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblServiceZone].[serviceZoneID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblServiceZone', @level2type = N'INDEX', @level2name = N'tblServiceZone$serviceZoneID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblServiceZone]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblServiceZone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblServiceZone].[serviceZoneID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblServiceZone', @level2type = N'COLUMN', @level2name = N'serviceZoneID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblServiceZone].[serviceZone]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblServiceZone', @level2type = N'COLUMN', @level2name = N'serviceZone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblServiceZone].[PrimaryKey]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblServiceZone', @level2type = N'CONSTRAINT', @level2name = N'tblServiceZone$PrimaryKey';

