﻿CREATE TABLE [dbo].[MM] (
    [Service Zone]         NVARCHAR (50)  NULL,
    [Service District]     NVARCHAR (255) NULL,
    [DEALER CODE]          NVARCHAR (50)  NULL,
    [DEALER NAME]          NVARCHAR (255) NULL,
    [METRO NAME]           NVARCHAR (255) NULL,
    [ASA]                  NVARCHAR (255) NULL,
    [ACTIVE UIO BY DEALER] NVARCHAR (50)  NULL,
    [INACTIVE UIO BY ASA]  NVARCHAR (50)  NULL,
    [NEW UIO BY DEALER]    NVARCHAR (50)  NULL,
    [LAPSED UIO BY ASA]    NVARCHAR (50)  NULL,
    [Total UIO]            NVARCHAR (50)  NULL,
    [Market Share]         NVARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [MM$DEALER CODE]
    ON [dbo].[MM]([DEALER CODE] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[DEALER CODE]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'INDEX', @level2name = N'MM$DEALER CODE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[Service Zone]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'Service Zone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[Service District]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'Service District';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[DEALER CODE]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'DEALER CODE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[DEALER NAME]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'DEALER NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[METRO NAME]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'METRO NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[ASA]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'ASA';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[ACTIVE UIO BY DEALER]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'ACTIVE UIO BY DEALER';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[INACTIVE UIO BY ASA]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'INACTIVE UIO BY ASA';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[NEW UIO BY DEALER]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'NEW UIO BY DEALER';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[LAPSED UIO BY ASA]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'LAPSED UIO BY ASA';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[Total UIO]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'Total UIO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[MM].[Market Share]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MM', @level2type = N'COLUMN', @level2name = N'Market Share';

