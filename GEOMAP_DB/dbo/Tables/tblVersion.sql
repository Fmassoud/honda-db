﻿CREATE TABLE [dbo].[tblVersion] (
    [fldDataDate] DATETIME2 (0) NULL,
    [fldVersion]  NVARCHAR (50) NULL,
    CONSTRAINT [SSMA_CC$tblVersion$fldVersion$disallow_zero_length] CHECK (len([fldVersion])>(0))
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblVersion]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblVersion';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblVersion].[fldDataDate]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblVersion', @level2type = N'COLUMN', @level2name = N'fldDataDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblVersion].[fldVersion]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblVersion', @level2type = N'COLUMN', @level2name = N'fldVersion';

