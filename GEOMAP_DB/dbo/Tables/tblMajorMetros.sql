﻿CREATE TABLE [dbo].[tblMajorMetros] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [zip]            NVARCHAR (255) NULL,
    [majorMetroID]   NVARCHAR (255) NULL,
    [majorMetroName] NVARCHAR (255) NULL,
    CONSTRAINT [tblMajorMetros$PrimaryKey] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMajorMetros]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMajorMetros';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMajorMetros].[ID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMajorMetros', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMajorMetros].[zip]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMajorMetros', @level2type = N'COLUMN', @level2name = N'zip';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMajorMetros].[majorMetroID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMajorMetros', @level2type = N'COLUMN', @level2name = N'majorMetroID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMajorMetros].[majorMetroName]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMajorMetros', @level2type = N'COLUMN', @level2name = N'majorMetroName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblMajorMetros].[PrimaryKey]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblMajorMetros', @level2type = N'CONSTRAINT', @level2name = N'tblMajorMetros$PrimaryKey';

