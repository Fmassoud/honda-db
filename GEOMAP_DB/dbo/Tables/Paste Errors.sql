﻿CREATE TABLE [dbo].[Paste Errors] (
    [F1]  NVARCHAR (255) NULL,
    [F2]  NVARCHAR (255) NULL,
    [F3]  NVARCHAR (255) NULL,
    [F4]  NVARCHAR (255) NULL,
    [F5]  NVARCHAR (255) NULL,
    [F6]  NVARCHAR (255) NULL,
    [F7]  NVARCHAR (255) NULL,
    [F8]  NVARCHAR (255) NULL,
    [F9]  NVARCHAR (255) NULL,
    [F10] NVARCHAR (255) NULL,
    CONSTRAINT [SSMA_CC$Paste Errors$F1$disallow_zero_length] CHECK (len([F1])>(0)),
    CONSTRAINT [SSMA_CC$Paste Errors$F10$disallow_zero_length] CHECK (len([F10])>(0)),
    CONSTRAINT [SSMA_CC$Paste Errors$F2$disallow_zero_length] CHECK (len([F2])>(0)),
    CONSTRAINT [SSMA_CC$Paste Errors$F3$disallow_zero_length] CHECK (len([F3])>(0)),
    CONSTRAINT [SSMA_CC$Paste Errors$F4$disallow_zero_length] CHECK (len([F4])>(0)),
    CONSTRAINT [SSMA_CC$Paste Errors$F5$disallow_zero_length] CHECK (len([F5])>(0)),
    CONSTRAINT [SSMA_CC$Paste Errors$F6$disallow_zero_length] CHECK (len([F6])>(0)),
    CONSTRAINT [SSMA_CC$Paste Errors$F7$disallow_zero_length] CHECK (len([F7])>(0)),
    CONSTRAINT [SSMA_CC$Paste Errors$F8$disallow_zero_length] CHECK (len([F8])>(0)),
    CONSTRAINT [SSMA_CC$Paste Errors$F9$disallow_zero_length] CHECK (len([F9])>(0))
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors].[F1]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors', @level2type = N'COLUMN', @level2name = N'F1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors].[F2]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors', @level2type = N'COLUMN', @level2name = N'F2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors].[F3]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors', @level2type = N'COLUMN', @level2name = N'F3';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors].[F4]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors', @level2type = N'COLUMN', @level2name = N'F4';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors].[F5]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors', @level2type = N'COLUMN', @level2name = N'F5';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors].[F6]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors', @level2type = N'COLUMN', @level2name = N'F6';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors].[F7]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors', @level2type = N'COLUMN', @level2name = N'F7';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors].[F8]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors', @level2type = N'COLUMN', @level2name = N'F8';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors].[F9]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors', @level2type = N'COLUMN', @level2name = N'F9';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[Paste Errors].[F10]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Paste Errors', @level2type = N'COLUMN', @level2name = N'F10';

