﻿CREATE TABLE [dbo].[tblDealers] (
    [dealerID]        INT           NULL,
    [dealerName]      NVARCHAR (30) NULL,
    [address]         NVARCHAR (30) NULL,
    [city]            NVARCHAR (30) NULL,
    [state]           NVARCHAR (2)  NULL,
    [zip5]            NVARCHAR (5)  NULL,
    [zip4]            NVARCHAR (4)  NULL,
    [serviceZone]     INT           NULL,
    [serviceDistrict] NVARCHAR (1)  NULL,
    [prodDivision]    NVARCHAR (3)  NULL,
    [AssnPC]          INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [tblDealers$dealerID]
    ON [dbo].[tblDealers]([dealerID] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[dealerID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'INDEX', @level2name = N'tblDealers$dealerID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[dealerID]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'dealerID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[dealerName]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'dealerName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[address]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'address';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[city]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[state]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'state';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[zip5]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'zip5';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[zip4]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'zip4';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[serviceZone]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'serviceZone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[serviceDistrict]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'serviceDistrict';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[prodDivision]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'prodDivision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'geomap.[tblDealers].[AssnPC]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tblDealers', @level2type = N'COLUMN', @level2name = N'AssnPC';

