﻿create procedure [qryDetermineLatLon] as 
begin
SELECT DISTINCT Avg(tblZip.longitude) AS AvgOflongitude, Avg(tblZip.latitude) AS AvgOflatitude, tblDealers.prodDivision, tblDealers.serviceZone, tblDealers.serviceDistrict INTO latlon
FROM tblZip INNER JOIN tblDealers ON tblZip.zip = tblDealers.zip5
GROUP BY tblDealers.prodDivision, tblDealers.serviceZone, tblDealers.serviceDistrict
ORDER BY tblDealers.prodDivision, tblDealers.serviceZone, tblDealers.serviceDistrict;

select * from latlon
end