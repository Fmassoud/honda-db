﻿create procedure [qryImportDataInfo] as 
begin
INSERT INTO tblData ( ahmDivision, prodDivision, dealerID, dptsID, jobCode, moduleID, lastName, firstName, midName, dateEligible, email, dateeligibletext )
SELECT GOMAPTRNG.AHMDiv, GOMAPTRNG.prodDiv, GOMAPTRNG.dealerID, GOMAPTRNG.dptsID, GOMAPTRNG.JobCode, GOMAPTRNG.ModuleID, GOMAPTRNG.last, GOMAPTRNG.first, GOMAPTRNG.Middle, GOMAPTRNG.eligdate, GOMAPTRNG.email,
 Year([eligdate]) & 
 case when Len(Month([eligdate]))=1 then "0" & Month([eligdate]) else Month([eligdate]) end
 & 
 case when Len(Day([eligdate]))=1 then "0" & Day([eligdate]) else Day([eligdate]) end
  
   AS eligdatetxt
FROM GOMAPTRNG;
select * from tblData
end