﻿create procedure [qryImportDealerInfo]  as 
begin

INSERT INTO tblDealers ( dealerID, dealerName, address, city, state, zip5, zip4, serviceZone, prodDivision, serviceDistrict, AssnPC )
SELECT GOMAPDLR.DealerID, GOMAPDLR.DealerName, GOMAPDLR.Address, GOMAPDLR.City, GOMAPDLR.State, GOMAPDLR.Zip5, GOMAPDLR.zip4, GOMAPDLR.svcZoneRef, GOMAPDLR.prodDivRef, GOMAPDLR.svcDistRef, GOMAPDLR.AssnPC
FROM GOMAPDLR;

select * from tblDealers
end