﻿CREATE TABLE [dbo].[zipShapeCensus] (
    [ZCTA5CE10]  NVARCHAR (255)   NULL,
    [GEOID10]    INT              NULL,
    [CLASSFP10]  NVARCHAR (255)   NULL,
    [MTFCC10]    NVARCHAR (255)   NULL,
    [FUNCSTAT10] NVARCHAR (255)   NULL,
    [ALAND10]    BIGINT           NULL,
    [AWATER10]   BIGINT           NULL,
    [INTPTLAT10] NVARCHAR (255)   NULL,
    [INTPTLON10] NVARCHAR (255)   NULL,
    [geom]       [sys].[geometry] NULL,
    [CustomLAT]  VARCHAR (500)    NULL
);

