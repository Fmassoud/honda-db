﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_getDealers]
	-- Add the parameters for the stored procedure here
	@product varchar(1) = 'a',
	@zone varchar(2) = '%', 
	@district varchar(2) = '%', 
	@financeRegion varchar(5) = '%', 
	@financeDistrict varchar(5) = '%' 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @product = 'a' 
		SELECT DISTINCT dealerCode, dealerName FROM tbl_hondaDealerTable WHERE salesZoneCode LIKE @zone and salesDistrictCode LIKE @district and financeRegionCode LIKE @financeRegion and financeDistrictCode LIKE @financeDistrict ORDER BY dealerCode
	ELSE
		SELECT DISTINCT dealerCode, dealerName FROM tbl_acuraDealerTable WHERE salesZoneCode LIKE @zone and salesDistrictCode LIKE @district and financeRegionCode LIKE @financeRegion and financeDistrictCode LIKE @financeDistrict ORDER BY dealerCode
END
