﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetConquestData]
		@Where varchar(3000) = '',
		@tablePrefix varchar(300) = ''
AS
BEGIN
	DECLARE @Select varchar(1000)
	DECLARE @FullStatement nvarchar(3000)
	SET @Select = 'SELECT SUM(conquestCount) AS conquestCount FROM  tbl_' + @tablePrefix + 'Conquest WHERE 1 = 1'
	SET @FullStatement = @Select + @Where
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
      EXECUTE sp_executesql @FullStatement
	
END
