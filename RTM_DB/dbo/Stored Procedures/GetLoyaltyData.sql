﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLoyaltyData]
		@Where varchar(3000) = '',
		@tablePrefix varchar(300) = ''
AS
BEGIN
	DECLARE @Select varchar(3000)
	DECLARE @FullStatement nvarchar(4000)
	SET @Select = 'SELECT SUM(returnToMarket) AS returnToMarket, 
		SUM(dealerLoyaltyCount) AS dealerLoyaltyCount, 
		SUM(otherDealerCount) AS otherDealerCount, 
		SUM(brandLoyaltyCount) AS brandLoyaltyCount,                       
        SUM(defectionCompetitiveSegmentCount) AS defectionCompetitiveSegmentCount, 
        SUM(defectionNonCompetitiveSegmentCount)AS defectionNonCompetitiveSegmentCount, 
        SUM(lostOpportunityCount) AS lostOpportunityCount, 
        SUM(captiveLoyalReturnToMarketCount) AS Captive_RTM, 
        SUM(captiveLoyalCount) AS Captive_Loyal, 
        SUM(captiveLeaseReturnToMarketCount) AS Captive_Lease_RTM, 
        SUM(captiveLeaseCount) AS Captive_Lease_Loyal, 
        SUM(captiveRetailRTM) AS Captive_Retail_RTM, 
        SUM(captiveRetailLoyalCount) AS Captive_Retail_Loyal, 
        SUM(nonCaptiveRTM) AS Non_Captive_RTM, 
        SUM(nonCaptiveLoyalCount) AS Non_Captive_Loyal,
        SUM(nonCaptiveLeaseRTMCount) AS Non_Captive_Lease_RTM, 
		SUM(nonCaptiveLeaseLoyalCount) AS Non_Captive_Lease_Loyal,
		SUM(nonCaptiveRetailRTM) AS Non_Captive_Retail_RTM, 
		SUM(nonCaptiveRetailLoyalCount) AS Non_Captive_Retail_Loyal,
		SUM(unknownRTM) AS Unknown_RTM,
		SUM(unknownLoyalCount) AS Unknown_Loyal,
		SUM(unknownLeaseRTMCount) AS Unknown_Lease_RTM,
		SUM(unknownLeaseLoyalCount) AS Unknown_Lease_Loyal,
		SUM(unknownRetailRTM) AS Unknown_Retail_RTM,
		SUM(unknownRetailLoyalCount) AS Unknown_Retail_Loyal
                    FROM tbl_' + @tablePrefix + 'Loyalty WHERE 1 = 1 '
	SET @FullStatement = @Select + @Where
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
      EXECUTE sp_executesql @FullStatement
	
END
