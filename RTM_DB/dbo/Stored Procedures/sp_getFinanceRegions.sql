﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_getFinanceRegions]
	-- Add the parameters for the stored procedure here
	@product varchar(1) = 'a',
	@zone varchar(2) = '%', 
	@district varchar(2) = '%'
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @product = 'a' 
		SELECT DISTINCT financeRegionCode, financeRegionName FROM tbl_hondaDealerTable WHERE salesZoneCode LIKE @zone and salesDistrictCode LIKE @district ORDER BY financeRegionName
	ELSE
		SELECT DISTINCT financeRegionCode, financeRegionName FROM tbl_acuraDealerTable WHERE salesZoneCode LIKE @zone and salesDistrictCode LIKE @district ORDER BY financeRegionName
END
