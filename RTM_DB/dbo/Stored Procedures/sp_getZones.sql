﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_getZones] 
	-- Add the parameters for the stored procedure here
	@product varchar(1) = 'a'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @product = 'a' 
		SELECT DISTINCT salesZoneCode, salesZoneName FROM tbl_hondaDealerTable ORDER BY salesZoneName
	ELSE
		SELECT DISTINCT salesZoneCode, salesZoneName FROM tbl_acuraDealerTable ORDER BY salesZoneName
	
END
