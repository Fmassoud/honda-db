﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_getDistricts]
	-- Add the parameters for the stored procedure here
	@product varchar(1) = 'a',
	@zone varchar(2) = '' 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @product = 'a' 
		SELECT DISTINCT salesDistrictCode FROM tbl_hondaDealerTable WHERE salesZoneCode = @zone ORDER BY salesDistrictCode
	ELSE
		SELECT DISTINCT salesDistrictCode FROM tbl_acuraDealerTable WHERE salesZoneCode = @zone ORDER BY salesDistrictCode
END
