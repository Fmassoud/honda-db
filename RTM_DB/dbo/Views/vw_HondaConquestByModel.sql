﻿CREATE VIEW [dbo].[vw_HondaConquestByModel]
AS
SELECT     dbo.tbl_HondaConquest.conquestedFromMake, dbo.tbl_HondaConquest.conquestedFromModel, dbo.tbl_HondaConquest.salesZoneCode, 
                      dbo.tbl_HondaConquest.salesDistrictCode, dbo.tbl_HondaConquest.financeRegionCode, dbo.tbl_HondaConquest.financeRegionName, 
                      dbo.tbl_HondaConquest.financeDistrictCode, dbo.tbl_HondaConquest.dealerCode, dbo.tbl_HondaConquest.dealerName, dbo.tbl_HondaConquest.reportYearAndMonth, 
                      dbo.tbl_HondaConquest.make, dbo.tbl_HondaConquest.model, dbo.tbl_HondaConquest.conquestCount - dbo.tbl_HondaDefection.defectionCount AS net, 
                      dbo.tbl_HondaConquest.conquestCount, dbo.tbl_HondaDefection.defectionCount
FROM         dbo.tbl_HondaConquest INNER JOIN
                      dbo.tbl_HondaDefection ON dbo.tbl_HondaConquest.salesZoneCode = dbo.tbl_HondaDefection.salesZoneCode AND 
                      dbo.tbl_HondaConquest.salesDistrictCode = dbo.tbl_HondaDefection.salesDistrictCode AND 
                      dbo.tbl_HondaConquest.financeRegionCode = dbo.tbl_HondaDefection.financeRegionCode AND 
                      dbo.tbl_HondaConquest.financeRegionName = dbo.tbl_HondaDefection.financeRegionName AND 
                      dbo.tbl_HondaConquest.financeDistrictCode = dbo.tbl_HondaDefection.financeDistrictCode AND 
                      dbo.tbl_HondaConquest.dealerCode = dbo.tbl_HondaDefection.dealerCode AND dbo.tbl_HondaConquest.dealerName = dbo.tbl_HondaDefection.dealerName AND 
                      dbo.tbl_HondaConquest.reportYearAndMonth = dbo.tbl_HondaDefection.reportYearAndMonth AND dbo.tbl_HondaConquest.model = dbo.tbl_HondaDefection.model AND
                       dbo.tbl_HondaConquest.make = dbo.tbl_HondaDefection.make AND dbo.tbl_HondaConquest.conquestedFromMake = dbo.tbl_HondaDefection.defectedToMake AND 
                      dbo.tbl_HondaConquest.conquestedFromModel = dbo.tbl_HondaDefection.defectedToModel
WHERE     (dbo.tbl_HondaConquest.conquestedFromMake NOT LIKE '%acura%') AND (dbo.tbl_HondaConquest.conquestedFromMake NOT LIKE '%honda%')

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_HondaConquest"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_HondaDefection"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_HondaConquestByModel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_HondaConquestByModel';

