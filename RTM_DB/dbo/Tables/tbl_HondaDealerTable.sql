﻿CREATE TABLE [dbo].[tbl_HondaDealerTable] (
    [dealerCode]          VARCHAR (6)   NOT NULL,
    [dealerName]          VARCHAR (40)  NULL,
    [dealerCity]          VARCHAR (50)  NULL,
    [stateAbbreviation]   VARCHAR (2)   NULL,
    [division]            VARCHAR (5)   NULL,
    [financeRegionCode]   VARCHAR (3)   NULL,
    [financeRegionName]   VARCHAR (16)  NULL,
    [financeDistrictCode] VARCHAR (50)  NULL,
    [salesZoneCode]       VARCHAR (2)   NULL,
    [salesZoneName]       VARCHAR (15)  NULL,
    [salesDistrictCode]   VARCHAR (800) NULL,
    CONSTRAINT [PK_tbl_dealer] PRIMARY KEY CLUSTERED ([dealerCode] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaDealer_13_1797581442__K1]
    ON [dbo].[tbl_HondaDealerTable]([dealerCode] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaDealer_13_1797581442__K6_K1]
    ON [dbo].[tbl_HondaDealerTable]([financeRegionCode] ASC, [dealerCode] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaDealer_13_1797581442__K7_K6_K9_K11]
    ON [dbo].[tbl_HondaDealerTable]([financeRegionName] ASC, [financeRegionCode] ASC, [salesZoneCode] ASC, [salesDistrictCode] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_HondaDealer_AHFC]
    ON [dbo].[tbl_HondaDealerTable]([dealerCode] ASC, [financeRegionCode] ASC, [financeRegionName] ASC, [financeDistrictCode] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_HondaDealer_AHM]
    ON [dbo].[tbl_HondaDealerTable]([dealerCode] ASC, [salesZoneCode] ASC, [salesZoneName] ASC, [salesDistrictCode] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_HondaDealer_City_State]
    ON [dbo].[tbl_HondaDealerTable]([dealerCode] ASC, [dealerCity] ASC, [stateAbbreviation] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_HondaDealer_Dealer]
    ON [dbo].[tbl_HondaDealerTable]([dealerCode] ASC, [dealerName] ASC) WITH (FILLFACTOR = 90);


GO
CREATE STATISTICS [_dta_stat_101575400_10_9]
    ON [dbo].[tbl_HondaDealerTable]([salesZoneName], [salesZoneCode]);


GO
CREATE STATISTICS [_dta_stat_101575400_11_1]
    ON [dbo].[tbl_HondaDealerTable]([salesDistrictCode], [dealerCode]);


GO
CREATE STATISTICS [_dta_stat_101575400_9_11]
    ON [dbo].[tbl_HondaDealerTable]([salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_101575400_1_9_11]
    ON [dbo].[tbl_HondaDealerTable]([dealerCode], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_101575400_4_1_9_11]
    ON [dbo].[tbl_HondaDealerTable]([stateAbbreviation], [dealerCode], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_101575400_6_9_11_8]
    ON [dbo].[tbl_HondaDealerTable]([financeRegionCode], [salesZoneCode], [salesDistrictCode], [financeDistrictCode]);

