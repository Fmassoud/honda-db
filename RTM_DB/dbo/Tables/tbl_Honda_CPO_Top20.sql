﻿CREATE TABLE [dbo].[tbl_Honda_CPO_Top20] (
    [DealerCode] NVARCHAR (6) NOT NULL,
    CONSTRAINT [PK_tbl_Honda_CPO_Top20] PRIMARY KEY CLUSTERED ([DealerCode] ASC) WITH (FILLFACTOR = 90)
);

