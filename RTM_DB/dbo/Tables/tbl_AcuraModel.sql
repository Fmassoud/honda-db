﻿CREATE TABLE [dbo].[tbl_AcuraModel] (
    [salesZoneCode]       VARCHAR (2)  NULL,
    [salesDistrictCode]   VARCHAR (1)  NULL,
    [financeRegionCode]   VARCHAR (3)  NULL,
    [financeRegionName]   VARCHAR (50) NULL,
    [financeDistrictCode] VARCHAR (2)  NULL,
    [dealerCode]          VARCHAR (6)  NOT NULL,
    [dealerName]          VARCHAR (40) NULL,
    [reportYearAndMonth]  VARCHAR (6)  NOT NULL,
    [make]                VARCHAR (30) NULL,
    [model]               VARCHAR (30) NULL,
    [returnToMarketCount] INT          NULL,
    [dealerLoyaltyCount]  INT          NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_tbl_AcuraModel_COMMON]
    ON [dbo].[tbl_AcuraModel]([dealerCode] ASC, [dealerName] ASC, [reportYearAndMonth] ASC, [make] ASC, [model] ASC, [returnToMarketCount] ASC, [dealerLoyaltyCount] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraModel_AHFC]
    ON [dbo].[tbl_AcuraModel]([financeRegionCode] ASC, [financeRegionName] ASC, [financeDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraModel_AHM]
    ON [dbo].[tbl_AcuraModel]([salesZoneCode] ASC, [salesDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraModel_Auto]
    ON [dbo].[tbl_AcuraModel]([dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);

