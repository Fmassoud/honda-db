﻿CREATE TABLE [dbo].[tbl_AcuraTop20] (
    [dealerCode] VARCHAR (6) NOT NULL,
    CONSTRAINT [PK_tbl_acuraTop20] PRIMARY KEY CLUSTERED ([dealerCode] ASC) WITH (FILLFACTOR = 90)
);

