﻿CREATE TABLE [dbo].[tbl_AcuraLoyalty] (
    [salesZoneCode]                       VARCHAR (2)  NULL,
    [salesDistrictCode]                   VARCHAR (1)  NULL,
    [financeRegionCode]                   VARCHAR (3)  NULL,
    [financeRegionName]                   VARCHAR (16) NULL,
    [financeDistrictCode]                 VARCHAR (2)  NULL,
    [dealerCode]                          VARCHAR (6)  NOT NULL,
    [dealerName]                          VARCHAR (40) NULL,
    [reportYearAndMonth]                  VARCHAR (6)  NOT NULL,
    [returnToMarket]                      INT          NULL,
    [dealerLoyaltyCount]                  INT          NULL,
    [otherDealerCount]                    INT          NULL,
    [brandLoyaltyCount]                   INT          NULL,
    [defectionCompetitiveSegmentCount]    INT          NULL,
    [defectionNonCompetitiveSegmentCount] INT          NULL,
    [lostOpportunityCount]                INT          NULL,
    [captiveLoyalReturnToMarketCount]     INT          NULL,
    [captiveLoyalCount]                   INT          NULL,
    [captiveLeaseReturnToMarketCount]     INT          NULL,
    [captiveLeaseCount]                   INT          NULL,
    [captiveRetailRTM]                    INT          NULL,
    [captiveRetailLoyalCount]             INT          NULL,
    [nonCaptiveRTM]                       INT          NULL,
    [nonCaptiveLoyalCount]                INT          NULL,
    [nonCaptiveLeaseRTMCount]             INT          NULL,
    [nonCaptiveLeaseLoyalCount]           INT          NULL,
    [nonCaptiveRetailRTM]                 INT          NULL,
    [nonCaptiveRetailLoyalCount]          INT          NULL,
    [unknownRTM]                          INT          NULL,
    [unknownLoyalCount]                   INT          NULL,
    [unknownLeaseRTMCount]                INT          NULL,
    [unknownLeaseLoyalCount]              INT          NULL,
    [unknownRetailRTM]                    INT          NULL,
    [unknownRetailLoyalCount]             INT          NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_AcuraLoyalty_COMMON]
    ON [dbo].[tbl_AcuraLoyalty]([dealerCode] ASC, [dealerName] ASC, [reportYearAndMonth] ASC, [returnToMarket] ASC, [dealerLoyaltyCount] ASC, [otherDealerCount] ASC, [brandLoyaltyCount] ASC, [defectionCompetitiveSegmentCount] ASC, [defectionNonCompetitiveSegmentCount] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraLoyalty_AHFC]
    ON [dbo].[tbl_AcuraLoyalty]([financeRegionCode] ASC, [financeRegionName] ASC, [financeDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraLoyalty_AHM]
    ON [dbo].[tbl_AcuraLoyalty]([salesZoneCode] ASC, [salesDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraLoyalty_Auto]
    ON [dbo].[tbl_AcuraLoyalty]([dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);

