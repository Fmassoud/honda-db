﻿CREATE TABLE [dbo].[tbl_HondaDefection] (
    [salesZoneCode]               VARCHAR (2)  NULL,
    [salesDistrictCode]           VARCHAR (1)  NULL,
    [financeRegionCode]           VARCHAR (3)  NULL,
    [financeRegionName]           VARCHAR (16) NULL,
    [financeDistrictCode]         VARCHAR (2)  NULL,
    [dealerCode]                  VARCHAR (6)  NOT NULL,
    [dealerName]                  VARCHAR (40) NULL,
    [reportYearAndMonth]          VARCHAR (6)  NOT NULL,
    [make]                        VARCHAR (30) NULL,
    [model]                       VARCHAR (30) NULL,
    [defectedToMake]              VARCHAR (30) NULL,
    [defectedToModel]             VARCHAR (50) NULL,
    [defectionCount]              INT          NULL,
    [competitiveSegmentIndicator] VARCHAR (1)  NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_HondaDefection_COMMON]
    ON [dbo].[tbl_HondaDefection]([dealerCode] ASC, [dealerName] ASC, [reportYearAndMonth] ASC, [model] ASC, [defectedToMake] ASC, [defectedToModel] ASC, [defectionCount] ASC, [competitiveSegmentIndicator] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaDefection_13_1317579732__K10]
    ON [dbo].[tbl_HondaDefection]([model] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaDefection_13_1317579732__K11_K8]
    ON [dbo].[tbl_HondaDefection]([defectedToMake] ASC, [reportYearAndMonth] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaDefection_13_1317579732__K14_K8_K11_10_13]
    ON [dbo].[tbl_HondaDefection]([competitiveSegmentIndicator] ASC, [reportYearAndMonth] ASC, [defectedToMake] ASC)
    INCLUDE([model], [defectionCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaDefection_13_1317579732__K8]
    ON [dbo].[tbl_HondaDefection]([reportYearAndMonth] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaDefection_AHFC]
    ON [dbo].[tbl_HondaDefection]([financeRegionCode] ASC, [financeRegionName] ASC, [financeDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaDefection_AHM]
    ON [dbo].[tbl_HondaDefection]([salesZoneCode] ASC, [salesDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaDefection_Auto]
    ON [dbo].[tbl_HondaDefection]([dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaDefection_Auto2]
    ON [dbo].[tbl_HondaDefection]([competitiveSegmentIndicator] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaDefection_Auto3]
    ON [dbo].[tbl_HondaDefection]([defectedToMake] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaDefection_7_85575343__K12_K11_K8_K10_13]
    ON [dbo].[tbl_HondaDefection]([defectedToModel] ASC, [defectedToMake] ASC, [reportYearAndMonth] ASC, [model] ASC)
    INCLUDE([defectionCount]);


GO
CREATE STATISTICS [_dta_stat_85575343_10_8]
    ON [dbo].[tbl_HondaDefection]([model], [reportYearAndMonth]);


GO
CREATE STATISTICS [_dta_stat_85575343_10_14]
    ON [dbo].[tbl_HondaDefection]([model], [competitiveSegmentIndicator]);


GO
CREATE STATISTICS [_dta_stat_85575343_11_8_1_2]
    ON [dbo].[tbl_HondaDefection]([defectedToMake], [reportYearAndMonth], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_8_10_1_2]
    ON [dbo].[tbl_HondaDefection]([reportYearAndMonth], [model], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_11_6_1_2]
    ON [dbo].[tbl_HondaDefection]([defectedToMake], [dealerCode], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_11_12_14_8]
    ON [dbo].[tbl_HondaDefection]([defectedToMake], [defectedToModel], [competitiveSegmentIndicator], [reportYearAndMonth]);


GO
CREATE STATISTICS [_dta_stat_85575343_11_8_10_1_2]
    ON [dbo].[tbl_HondaDefection]([defectedToMake], [reportYearAndMonth], [model], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_10_11_6_8_12]
    ON [dbo].[tbl_HondaDefection]([model], [defectedToMake], [dealerCode], [reportYearAndMonth], [defectedToModel]);


GO
CREATE STATISTICS [_dta_stat_85575343_14_8_10_1_2]
    ON [dbo].[tbl_HondaDefection]([competitiveSegmentIndicator], [reportYearAndMonth], [model], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_11_8_14_10_6]
    ON [dbo].[tbl_HondaDefection]([defectedToMake], [reportYearAndMonth], [competitiveSegmentIndicator], [model], [dealerCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_11_14_8_1_2]
    ON [dbo].[tbl_HondaDefection]([defectedToMake], [competitiveSegmentIndicator], [reportYearAndMonth], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_8_1_2_6_10_11]
    ON [dbo].[tbl_HondaDefection]([reportYearAndMonth], [salesZoneCode], [salesDistrictCode], [dealerCode], [model], [defectedToMake]);


GO
CREATE STATISTICS [_dta_stat_85575343_14_11_6_8_12_7]
    ON [dbo].[tbl_HondaDefection]([competitiveSegmentIndicator], [defectedToMake], [dealerCode], [reportYearAndMonth], [defectedToModel], [dealerName]);


GO
CREATE STATISTICS [_dta_stat_85575343_10_1_2_6_8_14]
    ON [dbo].[tbl_HondaDefection]([model], [salesZoneCode], [salesDistrictCode], [dealerCode], [reportYearAndMonth], [competitiveSegmentIndicator]);


GO
CREATE STATISTICS [_dta_stat_85575343_12_11_6_1_2_14]
    ON [dbo].[tbl_HondaDefection]([defectedToModel], [defectedToMake], [dealerCode], [salesZoneCode], [salesDistrictCode], [competitiveSegmentIndicator]);


GO
CREATE STATISTICS [_dta_stat_85575343_14_1_2_6_8_11_12]
    ON [dbo].[tbl_HondaDefection]([competitiveSegmentIndicator], [salesZoneCode], [salesDistrictCode], [dealerCode], [reportYearAndMonth], [defectedToMake], [defectedToModel]);


GO
CREATE STATISTICS [_dta_stat_85575343_11_14_8_10_1_2_6]
    ON [dbo].[tbl_HondaDefection]([defectedToMake], [competitiveSegmentIndicator], [reportYearAndMonth], [model], [salesZoneCode], [salesDistrictCode], [dealerCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_7_8_10_12_13_14_6_1]
    ON [dbo].[tbl_HondaDefection]([dealerName], [reportYearAndMonth], [model], [defectedToModel], [defectionCount], [competitiveSegmentIndicator], [dealerCode], [salesZoneCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_11_6_8_1_2_7_10_12_13]
    ON [dbo].[tbl_HondaDefection]([defectedToMake], [dealerCode], [reportYearAndMonth], [salesZoneCode], [salesDistrictCode], [dealerName], [model], [defectedToModel], [defectionCount]);


GO
CREATE STATISTICS [_dta_stat_85575343_14_6_7_8_10_11_12_13_1_2]
    ON [dbo].[tbl_HondaDefection]([competitiveSegmentIndicator], [dealerCode], [dealerName], [reportYearAndMonth], [model], [defectedToMake], [defectedToModel], [defectionCount], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_12_11_6_8_7]
    ON [dbo].[tbl_HondaDefection]([defectedToModel], [defectedToMake], [dealerCode], [reportYearAndMonth], [dealerName]);


GO
CREATE STATISTICS [_dta_stat_85575343_12_6_7_8_10]
    ON [dbo].[tbl_HondaDefection]([defectedToModel], [dealerCode], [dealerName], [reportYearAndMonth], [model]);


GO
CREATE STATISTICS [_dta_stat_85575343_12_11_8_1_2_6]
    ON [dbo].[tbl_HondaDefection]([defectedToModel], [defectedToMake], [reportYearAndMonth], [salesZoneCode], [salesDistrictCode], [dealerCode]);


GO
CREATE STATISTICS [_dta_stat_85575343_8_12]
    ON [dbo].[tbl_HondaDefection]([reportYearAndMonth], [defectedToModel]);


GO
CREATE STATISTICS [_dta_stat_85575343_8_11_6_7]
    ON [dbo].[tbl_HondaDefection]([reportYearAndMonth], [defectedToMake], [dealerCode], [dealerName]);

