﻿CREATE TABLE [dbo].[tbl_AcuraConquest] (
    [salesZoneCode]       VARCHAR (2)  NULL,
    [salesDistrictCode]   VARCHAR (1)  NULL,
    [financeRegionCode]   VARCHAR (3)  NULL,
    [financeRegionName]   VARCHAR (16) NULL,
    [financeDistrictCode] VARCHAR (2)  NULL,
    [dealerCode]          VARCHAR (6)  NOT NULL,
    [dealerName]          VARCHAR (40) NULL,
    [reportYearAndMonth]  VARCHAR (6)  NOT NULL,
    [make]                VARCHAR (30) NULL,
    [model]               VARCHAR (30) NULL,
    [conquestedFromMake]  VARCHAR (30) NULL,
    [conquestedFromModel] VARCHAR (30) NULL,
    [conquestCount]       INT          NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_AcuraConquest_COMMON]
    ON [dbo].[tbl_AcuraConquest]([dealerCode] ASC, [dealerName] ASC, [reportYearAndMonth] ASC, [make] ASC, [model] ASC, [conquestedFromMake] ASC, [conquestedFromModel] ASC, [conquestCount] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraConquest_AHFC]
    ON [dbo].[tbl_AcuraConquest]([financeRegionCode] ASC, [financeRegionName] ASC, [financeDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraConquest_AHM]
    ON [dbo].[tbl_AcuraConquest]([salesZoneCode] ASC, [salesDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraConquest_Auto]
    ON [dbo].[tbl_AcuraConquest]([dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);

