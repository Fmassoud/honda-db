﻿CREATE TABLE [dbo].[tbl_HondaTop20] (
    [dealerCode] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tbl_hondaTop20] PRIMARY KEY CLUSTERED ([dealerCode] ASC) WITH (FILLFACTOR = 90)
);

