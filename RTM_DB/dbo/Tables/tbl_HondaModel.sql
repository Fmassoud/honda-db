﻿CREATE TABLE [dbo].[tbl_HondaModel] (
    [salesZoneCode]       VARCHAR (2)  NULL,
    [salesDistrictCode]   VARCHAR (1)  NULL,
    [financeRegionCode]   VARCHAR (3)  NULL,
    [financeRegionName]   VARCHAR (16) NULL,
    [financeDistrictCode] VARCHAR (2)  NULL,
    [dealerCode]          VARCHAR (6)  NOT NULL,
    [dealerName]          VARCHAR (40) NULL,
    [reportYearAndMonth]  VARCHAR (6)  NOT NULL,
    [make]                VARCHAR (30) NULL,
    [model]               VARCHAR (30) NULL,
    [returnToMarketCount] INT          NULL,
    [dealerLoyaltyCount]  INT          NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_HondaModel_COMMON]
    ON [dbo].[tbl_HondaModel]([dealerCode] ASC, [dealerName] ASC, [reportYearAndMonth] ASC, [make] ASC, [model] ASC, [returnToMarketCount] ASC, [dealerLoyaltyCount] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaModel_13_725577623__K8]
    ON [dbo].[tbl_HondaModel]([reportYearAndMonth] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaModel_AHFC]
    ON [dbo].[tbl_HondaModel]([financeRegionCode] ASC, [financeRegionName] ASC, [financeDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaModel_AHM]
    ON [dbo].[tbl_HondaModel]([salesZoneCode] ASC, [salesDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaModel_Auto]
    ON [dbo].[tbl_HondaModel]([dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);

