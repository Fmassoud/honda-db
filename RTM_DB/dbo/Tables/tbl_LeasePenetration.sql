﻿CREATE TABLE [dbo].[tbl_LeasePenetration] (
    [DealerCode] VARCHAR (6)  NULL,
    [DealerName] VARCHAR (50) NULL,
    [TotalSales] INT          NULL,
    [LeaseSales] INT          NULL,
    [Day]        VARCHAR (2)  NULL,
    [Month]      VARCHAR (2)  NULL,
    [Year]       VARCHAR (4)  NULL
);

