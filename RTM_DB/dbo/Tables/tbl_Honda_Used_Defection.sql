﻿CREATE TABLE [dbo].[tbl_Honda_Used_Defection] (
    [SalesZoneCode]               VARCHAR (2)  NULL,
    [SalesDistrictCode]           VARCHAR (1)  NULL,
    [FinanceRegionCode]           VARCHAR (3)  NULL,
    [FinanceRegionName]           VARCHAR (16) NULL,
    [FinanceDistrictCode]         VARCHAR (2)  NULL,
    [DealerCode]                  VARCHAR (6)  NOT NULL,
    [DealerName]                  VARCHAR (40) NULL,
    [ReportYearAndMonth]          VARCHAR (6)  NOT NULL,
    [Make]                        VARCHAR (30) NULL,
    [Model]                       VARCHAR (30) NULL,
    [DefectedtoMake]              VARCHAR (30) NULL,
    [DefectedtoModel]             VARCHAR (50) NULL,
    [DefectionCount]              INT          NULL,
    [CompetitiveSegmentIndicator] VARCHAR (1)  NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_Honda_Used_Defection_COMMON]
    ON [dbo].[tbl_Honda_Used_Defection]([DealerCode] ASC, [DealerName] ASC, [ReportYearAndMonth] ASC, [Make] ASC, [Model] ASC, [DefectedtoMake] ASC, [DefectedtoModel] ASC, [DefectionCount] ASC, [CompetitiveSegmentIndicator] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_Honda_Used_Defection_13_98099390__K14_K8_13]
    ON [dbo].[tbl_Honda_Used_Defection]([CompetitiveSegmentIndicator] ASC, [ReportYearAndMonth] ASC)
    INCLUDE([DefectionCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_Honda_Used_Defection_13_98099390__K8]
    ON [dbo].[tbl_Honda_Used_Defection]([ReportYearAndMonth] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_Used_Defection_AHFC]
    ON [dbo].[tbl_Honda_Used_Defection]([FinanceRegionCode] ASC, [FinanceRegionName] ASC, [FinanceDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_Used_Defection_AHM]
    ON [dbo].[tbl_Honda_Used_Defection]([SalesZoneCode] ASC, [SalesDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_Used_Defection_Auto]
    ON [dbo].[tbl_Honda_Used_Defection]([DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_Honda_Used_Defection_7_213575799__K11_f48]
    ON [dbo].[tbl_Honda_Used_Defection]([DefectedtoMake] ASC) WHERE ([tbl_Honda_Used_Defection].[ReportYearAndMonth]>='201309' AND [tbl_Honda_Used_Defection].[ReportYearAndMonth]<='201408');


GO
CREATE STATISTICS [_dta_stat_213575799_8_11]
    ON [dbo].[tbl_Honda_Used_Defection]([ReportYearAndMonth], [DefectedtoMake]);


GO
CREATE STATISTICS [_dta_stat_213575799_11_12_14]
    ON [dbo].[tbl_Honda_Used_Defection]([DefectedtoMake], [DefectedtoModel], [CompetitiveSegmentIndicator]);


GO
CREATE STATISTICS [_dta_stat_213575799_14_8_11_12]
    ON [dbo].[tbl_Honda_Used_Defection]([CompetitiveSegmentIndicator], [ReportYearAndMonth], [DefectedtoMake], [DefectedtoModel]);

