﻿CREATE TABLE [dbo].[tbl_HondaConquest] (
    [salesZoneCode]       VARCHAR (2)  NULL,
    [salesDistrictCode]   VARCHAR (1)  NULL,
    [financeRegionCode]   VARCHAR (3)  NULL,
    [financeRegionName]   VARCHAR (16) NULL,
    [financeDistrictCode] VARCHAR (2)  NULL,
    [dealerCode]          VARCHAR (6)  NOT NULL,
    [dealerName]          VARCHAR (40) NULL,
    [reportYearAndMonth]  VARCHAR (6)  NOT NULL,
    [make]                VARCHAR (30) NULL,
    [model]               VARCHAR (30) NULL,
    [conquestedFromMake]  VARCHAR (30) NULL,
    [conquestedFromModel] VARCHAR (40) NULL,
    [conquestCount]       INT          NULL
);


GO
CREATE CLUSTERED INDEX [ix_reportYearAndMonth]
    ON [dbo].[tbl_HondaConquest]([reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaConquest_13_1861581670__K11_K8_13]
    ON [dbo].[tbl_HondaConquest]([conquestedFromMake] ASC, [reportYearAndMonth] ASC)
    INCLUDE([conquestCount]);


GO
CREATE NONCLUSTERED INDEX [ix_AHFC]
    ON [dbo].[tbl_HondaConquest]([financeRegionCode] ASC, [financeDistrictCode] ASC, [dealerCode] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_AHM]
    ON [dbo].[tbl_HondaConquest]([salesZoneCode] ASC, [salesDistrictCode] ASC, [dealerCode] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_salesZone]
    ON [dbo].[tbl_HondaConquest]([salesZoneCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_salesZoneDistrict]
    ON [dbo].[tbl_HondaConquest]([salesZoneCode] ASC, [salesDistrictCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_salesZoneDistrictDealer]
    ON [dbo].[tbl_HondaConquest]([salesZoneCode] ASC, [salesDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaConquest_7_133575514__K1_K2_K6_K8_K12_K11_13]
    ON [dbo].[tbl_HondaConquest]([salesZoneCode] ASC, [salesDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC, [conquestedFromModel] ASC, [conquestedFromMake] ASC)
    INCLUDE([conquestCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_HondaConquest_7_133575514__K10_K8]
    ON [dbo].[tbl_HondaConquest]([model] ASC, [reportYearAndMonth] ASC);


GO
CREATE STATISTICS [_dta_stat_133575514_8_10]
    ON [dbo].[tbl_HondaConquest]([reportYearAndMonth], [model]);


GO
CREATE STATISTICS [_dta_stat_133575514_8_12]
    ON [dbo].[tbl_HondaConquest]([reportYearAndMonth], [conquestedFromModel]);


GO
CREATE STATISTICS [_dta_stat_133575514_12_11_8]
    ON [dbo].[tbl_HondaConquest]([conquestedFromModel], [conquestedFromMake], [reportYearAndMonth]);


GO
CREATE STATISTICS [_dta_stat_133575514_11_6_1_2]
    ON [dbo].[tbl_HondaConquest]([conquestedFromMake], [dealerCode], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_133575514_6_1_2_8_11]
    ON [dbo].[tbl_HondaConquest]([dealerCode], [salesZoneCode], [salesDistrictCode], [reportYearAndMonth], [conquestedFromMake]);


GO
CREATE STATISTICS [_dta_stat_133575514_12_11_6_1_2]
    ON [dbo].[tbl_HondaConquest]([conquestedFromModel], [conquestedFromMake], [dealerCode], [salesZoneCode], [salesDistrictCode]);


GO
CREATE STATISTICS [_dta_stat_133575514_10_6_1_2_8]
    ON [dbo].[tbl_HondaConquest]([model], [dealerCode], [salesZoneCode], [salesDistrictCode], [reportYearAndMonth]);


GO
CREATE STATISTICS [_dta_stat_133575514_8_10_12]
    ON [dbo].[tbl_HondaConquest]([reportYearAndMonth], [model], [conquestedFromModel]);


GO
CREATE STATISTICS [_dta_stat_133575514_12_11_8_10]
    ON [dbo].[tbl_HondaConquest]([conquestedFromModel], [conquestedFromMake], [reportYearAndMonth], [model]);

