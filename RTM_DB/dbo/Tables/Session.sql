﻿CREATE TABLE [dbo].[Session] (
    [SessionID]        VARCHAR (300) NOT NULL,
    [IPAddress]        VARCHAR (20)  NOT NULL,
    [DealerID]         VARCHAR (15)  NULL,
    [ActingAsDealerID] VARCHAR (15)  NULL,
    [Tab]              VARCHAR (20)  NULL,
    [DateTimeStamp]    DATETIME      NOT NULL,
    [Username]         VARCHAR (50)  NULL,
    [Token]            VARCHAR (300) NULL,
    CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED ([SessionID] ASC) WITH (FILLFACTOR = 90)
);

