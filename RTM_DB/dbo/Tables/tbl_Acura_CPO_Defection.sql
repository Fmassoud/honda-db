﻿CREATE TABLE [dbo].[tbl_Acura_CPO_Defection] (
    [SalesZoneCode]               VARCHAR (2)  NULL,
    [SalesDistrictCode]           VARCHAR (1)  NULL,
    [FinanceRegionCode]           VARCHAR (3)  NULL,
    [FinanceRegionName]           VARCHAR (16) NULL,
    [FinanceDistrictCode]         VARCHAR (2)  NULL,
    [DealerCode]                  VARCHAR (6)  NOT NULL,
    [DealerName]                  VARCHAR (40) NULL,
    [ReportYearAndMonth]          VARCHAR (6)  NOT NULL,
    [Make]                        VARCHAR (30) NULL,
    [Model]                       VARCHAR (30) NULL,
    [DefectedtoMake]              VARCHAR (30) NULL,
    [DefectedtoModel]             VARCHAR (50) NULL,
    [DefectionCount]              INT          NULL,
    [CompetitiveSegmentIndicator] VARCHAR (1)  NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_Acura_CPO_Defection_COMMON]
    ON [dbo].[tbl_Acura_CPO_Defection]([DealerCode] ASC, [DealerName] ASC, [ReportYearAndMonth] ASC, [Make] ASC, [Model] ASC, [DefectedtoMake] ASC, [DefectedtoModel] ASC, [DefectionCount] ASC, [CompetitiveSegmentIndicator] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Acura_CPO_Defection_AHFC]
    ON [dbo].[tbl_Acura_CPO_Defection]([FinanceRegionCode] ASC, [FinanceRegionName] ASC, [FinanceDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Acura_CPO_Defection_AHM]
    ON [dbo].[tbl_Acura_CPO_Defection]([SalesZoneCode] ASC, [SalesDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Acura_CPO_Defection_Auto1]
    ON [dbo].[tbl_Acura_CPO_Defection]([DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);

