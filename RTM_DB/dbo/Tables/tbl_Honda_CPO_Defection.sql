﻿CREATE TABLE [dbo].[tbl_Honda_CPO_Defection] (
    [SalesZoneCode]               VARCHAR (2)  NULL,
    [SalesDistrictCode]           VARCHAR (1)  NULL,
    [FinanceRegionCode]           VARCHAR (3)  NULL,
    [FinanceRegionName]           VARCHAR (16) NULL,
    [FinanceDistrictCode]         VARCHAR (2)  NULL,
    [DealerCode]                  VARCHAR (6)  NOT NULL,
    [DealerName]                  VARCHAR (40) NULL,
    [ReportYearAndMonth]          VARCHAR (6)  NOT NULL,
    [Make]                        VARCHAR (30) NULL,
    [Model]                       VARCHAR (30) NULL,
    [DefectedtoMake]              VARCHAR (30) NULL,
    [DefectedtoModel]             VARCHAR (50) NULL,
    [DefectionCount]              INT          NULL,
    [CompetitiveSegmentIndicator] VARCHAR (1)  NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_Honda_CPO_Defection_COMMON]
    ON [dbo].[tbl_Honda_CPO_Defection]([DealerCode] ASC, [DealerName] ASC, [ReportYearAndMonth] ASC, [Make] ASC, [Model] ASC, [DefectedtoMake] ASC, [DefectedtoModel] ASC, [DefectionCount] ASC, [CompetitiveSegmentIndicator] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_Honda_CPO_Defection_13_82099333__K11_K12_K8_K14_13]
    ON [dbo].[tbl_Honda_CPO_Defection]([DefectedtoMake] ASC, [DefectedtoModel] ASC, [ReportYearAndMonth] ASC, [CompetitiveSegmentIndicator] ASC)
    INCLUDE([DefectionCount]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_Honda_CPO_Defection_13_82099333__K8_13_14]
    ON [dbo].[tbl_Honda_CPO_Defection]([ReportYearAndMonth] ASC)
    INCLUDE([DefectionCount], [CompetitiveSegmentIndicator]);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_CPO_Defection_AHFC]
    ON [dbo].[tbl_Honda_CPO_Defection]([FinanceRegionCode] ASC, [FinanceRegionName] ASC, [FinanceDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_CPO_Defection_AHM]
    ON [dbo].[tbl_Honda_CPO_Defection]([SalesZoneCode] ASC, [SalesDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_CPO_Defection_Auto]
    ON [dbo].[tbl_Honda_CPO_Defection]([DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE STATISTICS [_dta_stat_293576084_11_8]
    ON [dbo].[tbl_Honda_CPO_Defection]([DefectedtoMake], [ReportYearAndMonth]);


GO
CREATE STATISTICS [_dta_stat_293576084_8_14]
    ON [dbo].[tbl_Honda_CPO_Defection]([ReportYearAndMonth], [CompetitiveSegmentIndicator]);


GO
CREATE STATISTICS [_dta_stat_293576084_11_6]
    ON [dbo].[tbl_Honda_CPO_Defection]([DefectedtoMake], [DealerCode]);


GO
CREATE STATISTICS [_dta_stat_293576084_6_8_11]
    ON [dbo].[tbl_Honda_CPO_Defection]([DealerCode], [ReportYearAndMonth], [DefectedtoMake]);


GO
CREATE STATISTICS [_dta_stat_293576084_12_11_6_14]
    ON [dbo].[tbl_Honda_CPO_Defection]([DefectedtoModel], [DefectedtoMake], [DealerCode], [CompetitiveSegmentIndicator]);


GO
CREATE STATISTICS [_dta_stat_293576084_6_8_14_11_12]
    ON [dbo].[tbl_Honda_CPO_Defection]([DealerCode], [ReportYearAndMonth], [CompetitiveSegmentIndicator], [DefectedtoMake], [DefectedtoModel]);

