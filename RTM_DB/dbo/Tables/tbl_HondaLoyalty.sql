﻿CREATE TABLE [dbo].[tbl_HondaLoyalty] (
    [salesZoneCode]                       VARCHAR (2)  NULL,
    [salesDistrictCode]                   VARCHAR (1)  NULL,
    [financeRegionCode]                   VARCHAR (3)  NULL,
    [financeRegionName]                   VARCHAR (16) NULL,
    [financeDistrictCode]                 VARCHAR (2)  NULL,
    [dealerCode]                          VARCHAR (6)  NOT NULL,
    [dealerName]                          VARCHAR (40) NULL,
    [reportYearAndMonth]                  VARCHAR (6)  NOT NULL,
    [returnToMarket]                      INT          NULL,
    [dealerLoyaltyCount]                  INT          NULL,
    [otherDealerCount]                    INT          NULL,
    [brandLoyaltyCount]                   INT          NULL,
    [defectionCompetitiveSegmentCount]    INT          NULL,
    [defectionNonCompetitiveSegmentCount] INT          NULL,
    [lostOpportunityCount]                INT          NULL,
    [captiveLoyalReturnToMarketCount]     INT          NULL,
    [captiveLoyalCount]                   INT          NULL,
    [captiveLeaseReturnToMarketCount]     INT          NULL,
    [captiveLeaseCount]                   INT          NULL,
    [captiveRetailRTM]                    INT          NULL,
    [captiveRetailLoyalCount]             INT          NULL,
    [nonCaptiveRTM]                       INT          NULL,
    [nonCaptiveLoyalCount]                INT          NULL,
    [nonCaptiveLeaseRTMCount]             INT          NULL,
    [nonCaptiveLeaseLoyalCount]           INT          NULL,
    [nonCaptiveRetailRTM]                 INT          NULL,
    [nonCaptiveRetailLoyalCount]          INT          NULL,
    [unknownRTM]                          INT          NULL,
    [unknownLoyalCount]                   INT          NULL,
    [unknownLeaseRTMCount]                INT          NULL,
    [unknownLeaseLoyalCount]              INT          NULL,
    [unknownRetailRTM]                    INT          NULL,
    [unknownRetailLoyalCount]             INT          NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_HondaLoyalty_COMMON]
    ON [dbo].[tbl_HondaLoyalty]([dealerCode] ASC, [dealerName] ASC, [reportYearAndMonth] ASC, [returnToMarket] ASC, [dealerLoyaltyCount] ASC, [otherDealerCount] ASC, [brandLoyaltyCount] ASC, [defectionCompetitiveSegmentCount] ASC, [defectionNonCompetitiveSegmentCount] ASC, [lostOpportunityCount] ASC, [unknownRetailRTM] ASC, [unknownRTM] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaLoyalty_AHFC]
    ON [dbo].[tbl_HondaLoyalty]([financeRegionCode] ASC, [financeRegionName] ASC, [financeDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaLoyalty_AHM]
    ON [dbo].[tbl_HondaLoyalty]([salesZoneCode] ASC, [salesDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaLoyalty_Auto1]
    ON [dbo].[tbl_HondaLoyalty]([dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaLoyalty_Auto2]
    ON [dbo].[tbl_HondaLoyalty]([reportYearAndMonth] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_HondaLoyalty_Auto3]
    ON [dbo].[tbl_HondaLoyalty]([financeRegionCode] ASC, [reportYearAndMonth] ASC);


GO
CREATE STATISTICS [_dta_stat_69575286_1_8]
    ON [dbo].[tbl_HondaLoyalty]([salesZoneCode], [reportYearAndMonth]);


GO
CREATE STATISTICS [_dta_stat_69575286_2_8]
    ON [dbo].[tbl_HondaLoyalty]([salesDistrictCode], [reportYearAndMonth]);


GO
CREATE STATISTICS [_dta_stat_69575286_2_1_8]
    ON [dbo].[tbl_HondaLoyalty]([salesDistrictCode], [salesZoneCode], [reportYearAndMonth]);


GO
CREATE STATISTICS [_dta_stat_69575286_6_7_9_10_11_12_13_14_15_32_28]
    ON [dbo].[tbl_HondaLoyalty]([dealerCode], [dealerName], [returnToMarket], [dealerLoyaltyCount], [otherDealerCount], [brandLoyaltyCount], [defectionCompetitiveSegmentCount], [defectionNonCompetitiveSegmentCount], [lostOpportunityCount], [unknownRetailRTM], [unknownRTM]);


GO
CREATE STATISTICS [_dta_stat_69575286_8_1_6_7_9_10_11_12_13_14_15_32]
    ON [dbo].[tbl_HondaLoyalty]([reportYearAndMonth], [salesZoneCode], [dealerCode], [dealerName], [returnToMarket], [dealerLoyaltyCount], [otherDealerCount], [brandLoyaltyCount], [defectionCompetitiveSegmentCount], [defectionNonCompetitiveSegmentCount], [lostOpportunityCount], [unknownRetailRTM]);


GO
CREATE STATISTICS [_dta_stat_69575286_8_6_7_9_10_11_12_13_14_15_32_28_1]
    ON [dbo].[tbl_HondaLoyalty]([reportYearAndMonth], [dealerCode], [dealerName], [returnToMarket], [dealerLoyaltyCount], [otherDealerCount], [brandLoyaltyCount], [defectionCompetitiveSegmentCount], [defectionNonCompetitiveSegmentCount], [lostOpportunityCount], [unknownRetailRTM], [unknownRTM], [salesZoneCode]);

