﻿CREATE TABLE [dbo].[tbl_AcuraDefection] (
    [salesZoneCode]               VARCHAR (2)  NULL,
    [salesDistrictCode]           VARCHAR (1)  NULL,
    [financeRegionCode]           VARCHAR (3)  NULL,
    [financeRegionName]           VARCHAR (16) NULL,
    [financeDistrictCode]         VARCHAR (2)  NULL,
    [dealerCode]                  VARCHAR (6)  NOT NULL,
    [dealerName]                  VARCHAR (40) NULL,
    [reportYearAndMonth]          VARCHAR (6)  NOT NULL,
    [make]                        VARCHAR (30) NULL,
    [model]                       VARCHAR (30) NULL,
    [defectedToMake]              VARCHAR (30) NULL,
    [defectedToModel]             VARCHAR (30) NULL,
    [defectionCount]              INT          NULL,
    [competitiveSegmentIndicator] VARCHAR (1)  NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_AcuraDefection_COMMON]
    ON [dbo].[tbl_AcuraDefection]([dealerCode] ASC, [dealerName] ASC, [reportYearAndMonth] ASC, [make] ASC, [model] ASC, [defectedToMake] ASC, [defectedToModel] ASC, [defectionCount] ASC, [competitiveSegmentIndicator] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraDefection_AHFC]
    ON [dbo].[tbl_AcuraDefection]([financeRegionCode] ASC, [financeRegionName] ASC, [financeDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraDefection_AHM]
    ON [dbo].[tbl_AcuraDefection]([salesZoneCode] ASC, [salesDistrictCode] ASC, [dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_AcuraDefection_Auto]
    ON [dbo].[tbl_AcuraDefection]([dealerCode] ASC, [reportYearAndMonth] ASC) WITH (FILLFACTOR = 90);

