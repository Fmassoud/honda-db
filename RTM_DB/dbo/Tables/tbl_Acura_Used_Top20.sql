﻿CREATE TABLE [dbo].[tbl_Acura_Used_Top20] (
    [DealerCode] VARCHAR (6) NOT NULL,
    CONSTRAINT [PK_tbl_Acura_USED_Top20] PRIMARY KEY CLUSTERED ([DealerCode] ASC) WITH (FILLFACTOR = 90)
);

