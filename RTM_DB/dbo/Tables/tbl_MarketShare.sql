﻿CREATE TABLE [dbo].[tbl_MarketShare] (
    [ReportDate] DATETIME      NULL,
    [Zone]       VARCHAR (5)   NULL,
    [District]   VARCHAR (5)   NULL,
    [DealerName] VARCHAR (300) NULL,
    [DealerCode] VARCHAR (10)  NULL,
    [MPEN]       VARCHAR (10)  NULL,
    [SLPCT]      VARCHAR (10)  NULL,
    [id]         INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_tbl_MarketShare] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_MarketShare_Auto]
    ON [dbo].[tbl_MarketShare]([DealerCode] ASC, [ReportDate] ASC)
    INCLUDE([SLPCT]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_MarketShare_Zone]
    ON [dbo].[tbl_MarketShare]([Zone] ASC, [District] ASC) WITH (FILLFACTOR = 90);

