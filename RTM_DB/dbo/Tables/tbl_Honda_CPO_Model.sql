﻿CREATE TABLE [dbo].[tbl_Honda_CPO_Model] (
    [SalesZoneCode]       VARCHAR (2)  NULL,
    [SalesDistrictCode]   VARCHAR (1)  NULL,
    [FinanceRegionCode]   VARCHAR (3)  NULL,
    [FinanceRegionName]   VARCHAR (16) NULL,
    [FinanceDistrictCode] VARCHAR (2)  NULL,
    [DealerCode]          VARCHAR (6)  NOT NULL,
    [DealerName]          VARCHAR (40) NULL,
    [ReportYearAndMonth]  VARCHAR (6)  NOT NULL,
    [Make]                VARCHAR (30) NULL,
    [Model]               VARCHAR (30) NULL,
    [ReturnToMarketCount] INT          NULL,
    [DealerLoyaltyCount]  INT          NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_Honda_CPO_Model_COMMON]
    ON [dbo].[tbl_Honda_CPO_Model]([DealerCode] ASC, [DealerName] ASC, [ReportYearAndMonth] ASC, [Make] ASC, [Model] ASC, [ReturnToMarketCount] ASC, [DealerLoyaltyCount] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [_dta_index_tbl_Honda_CPO_Model_13_2133582639__K8_K10_11_12]
    ON [dbo].[tbl_Honda_CPO_Model]([ReportYearAndMonth] ASC, [Model] ASC)
    INCLUDE([ReturnToMarketCount], [DealerLoyaltyCount]);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_CPO_Model__AHM]
    ON [dbo].[tbl_Honda_CPO_Model]([SalesZoneCode] ASC, [SalesDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_CPO_Model_AHFC]
    ON [dbo].[tbl_Honda_CPO_Model]([FinanceRegionCode] ASC, [FinanceDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_CPO_Model_Auto]
    ON [dbo].[tbl_Honda_CPO_Model]([DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);

