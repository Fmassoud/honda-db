﻿CREATE TABLE [dbo].[tbl_AcuraDealerTable] (
    [dealerCode]          VARCHAR (6)   NOT NULL,
    [dealerName]          VARCHAR (40)  NULL,
    [dealerCity]          VARCHAR (50)  NULL,
    [stateAbbreviation]   VARCHAR (2)   NULL,
    [division]            VARCHAR (5)   NULL,
    [financeRegionCode]   VARCHAR (3)   NULL,
    [financeRegionName]   VARCHAR (16)  NULL,
    [financeDistrictCode] VARCHAR (50)  NULL,
    [salesZoneCode]       VARCHAR (2)   NULL,
    [salesZoneName]       VARCHAR (15)  NULL,
    [salesDistrictCode]   VARCHAR (800) NULL,
    CONSTRAINT [PK_tbl_acuraDealer] PRIMARY KEY CLUSTERED ([dealerCode] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_AcuraDealer_AHFC]
    ON [dbo].[tbl_AcuraDealerTable]([dealerCode] ASC, [financeRegionCode] ASC, [financeRegionName] ASC, [financeDistrictCode] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_AcuraDealer_AHM]
    ON [dbo].[tbl_AcuraDealerTable]([dealerCode] ASC, [salesZoneCode] ASC, [salesZoneName] ASC, [salesDistrictCode] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_AcuraDealer_City_STATE]
    ON [dbo].[tbl_AcuraDealerTable]([dealerCode] ASC, [dealerCity] ASC, [stateAbbreviation] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_AcuraDealer_Dealer]
    ON [dbo].[tbl_AcuraDealerTable]([dealerCode] ASC, [dealerName] ASC) WITH (FILLFACTOR = 90);

