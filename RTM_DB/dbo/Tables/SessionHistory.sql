﻿CREATE TABLE [dbo].[SessionHistory] (
    [sessionHistoryID]    INT           IDENTITY (1, 1) NOT NULL,
    [salesZoneCode]       VARCHAR (2)   NULL,
    [salesDistrictCode]   VARCHAR (2)   NULL,
    [financeRegionCode]   VARCHAR (3)   NULL,
    [financeRegionName]   VARCHAR (16)  NULL,
    [financeDistrictCode] VARCHAR (5)   NULL,
    [dealerCode]          VARCHAR (6)   NULL,
    [dealerName]          VARCHAR (40)  NULL,
    [reportYearAndMonth]  VARCHAR (50)  NULL,
    [fromDate]            VARCHAR (50)  NULL,
    [toDate]              VARCHAR (50)  NULL,
    [company]             VARCHAR (50)  NULL,
    [product]             VARCHAR (50)  NULL,
    [isTop20]             VARCHAR (50)  NULL,
    [isRolling3Months]    VARCHAR (50)  NULL,
    [RollingCount]        VARCHAR (50)  NULL,
    [SessionID]           VARCHAR (300) NULL,
    [dateTimeStamp]       DATETIME      CONSTRAINT [DF_SessionHistory_dateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [SessionUsername]     VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_SessionHistory] PRIMARY KEY CLUSTERED ([sessionHistoryID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_SessionHistory]
    ON [dbo].[SessionHistory]([SessionUsername] ASC) WITH (FILLFACTOR = 90);

