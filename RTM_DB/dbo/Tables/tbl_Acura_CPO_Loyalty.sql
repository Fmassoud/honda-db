﻿CREATE TABLE [dbo].[tbl_Acura_CPO_Loyalty] (
    [SalesZoneCode]                       VARCHAR (2)  NULL,
    [SalesDistrictCode]                   VARCHAR (1)  NULL,
    [FinanceRegionCode]                   VARCHAR (3)  NULL,
    [FinanceRegionName]                   VARCHAR (16) NULL,
    [FinanceDistrictCode]                 VARCHAR (2)  NULL,
    [DealerCode]                          VARCHAR (6)  NOT NULL,
    [DealerName]                          VARCHAR (40) NULL,
    [ReportYearAndMonth]                  VARCHAR (9)  NOT NULL,
    [ReturntoMarket]                      INT          NULL,
    [DealerLoyaltyCount]                  INT          NULL,
    [OtherDealerCount]                    INT          NULL,
    [BrandLoyaltyCount]                   INT          NULL,
    [DefectionCompetitiveSegmentCount]    INT          NULL,
    [DefectionNonCompetitiveSegmentCount] INT          NULL,
    [LostOpportunityCount]                INT          NULL,
    [CaptiveLoyalReturnToMarketCount]     INT          NULL,
    [CaptiveLoyalCount]                   INT          NULL,
    [CaptiveLeaseReturnToMarketCount]     INT          NULL,
    [CaptiveLeaseCount]                   INT          NULL,
    [CaptiveRetailRTM]                    INT          NULL,
    [CaptiveRetailLoyalCount]             INT          NULL,
    [NonCaptiveRTM]                       INT          NULL,
    [NonCaptiveLoyalCount]                INT          NULL,
    [NonCaptiveLeaseRTMCount]             INT          NULL,
    [NonCaptiveLeaseLoyalCount]           INT          NULL,
    [NonCaptiveRetailRTM]                 INT          NULL,
    [NonCaptiveRetailLoyalCount]          INT          NULL,
    [UnknownRTM]                          INT          NULL,
    [UnknownLoyalCount]                   INT          NULL,
    [UnknownLeaseRTMCount]                INT          NULL,
    [UnknownLeaseLoyalCount]              INT          NULL,
    [UnknownRetailRTM]                    INT          NULL,
    [UnknownRetailLoyalCount]             INT          NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_Acura_CPO_Loyalty_Common]
    ON [dbo].[tbl_Acura_CPO_Loyalty]([DealerCode] ASC, [DealerName] ASC, [ReportYearAndMonth] ASC, [ReturntoMarket] ASC, [DealerLoyaltyCount] ASC, [OtherDealerCount] ASC, [BrandLoyaltyCount] ASC, [DefectionCompetitiveSegmentCount] ASC, [DefectionNonCompetitiveSegmentCount] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Acura_CPO_Loyalty_AHFC]
    ON [dbo].[tbl_Acura_CPO_Loyalty]([FinanceRegionCode] ASC, [FinanceRegionName] ASC, [FinanceDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Acura_CPO_Loyalty_AHM]
    ON [dbo].[tbl_Acura_CPO_Loyalty]([SalesZoneCode] ASC, [SalesDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Acura_CPO_Loyalty_Auto]
    ON [dbo].[tbl_Acura_CPO_Loyalty]([DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);

