﻿CREATE TABLE [dbo].[tbl_ShowDialog] (
    [DealerID] VARCHAR (10) NULL,
    [Username] VARCHAR (50) NULL,
    [ShowFlag] INT          NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_ShowDialog_1]
    ON [dbo].[tbl_ShowDialog]([ShowFlag] ASC, [DealerID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_ShowDialog_2]
    ON [dbo].[tbl_ShowDialog]([DealerID] ASC, [Username] ASC) WITH (FILLFACTOR = 90);

