﻿CREATE TABLE [dbo].[tbl_Honda_CPO_Loyalty] (
    [SalesZoneCode]                       VARCHAR (2)  NULL,
    [SalesDistrictCode]                   VARCHAR (1)  NULL,
    [FinanceRegionCode]                   VARCHAR (3)  NULL,
    [FinanceRegionName]                   VARCHAR (16) NULL,
    [FinanceDistrictCode]                 VARCHAR (2)  NULL,
    [DealerCode]                          VARCHAR (6)  NOT NULL,
    [DealerName]                          VARCHAR (40) NULL,
    [ReportYearAndMonth]                  VARCHAR (9)  NOT NULL,
    [ReturntoMarket]                      INT          NULL,
    [DealerLoyaltyCount]                  INT          NULL,
    [OtherDealerCount]                    INT          NULL,
    [BrandLoyaltyCount]                   INT          NULL,
    [DefectionCompetitiveSegmentCount]    INT          NULL,
    [DefectionNonCompetitiveSegmentCount] INT          NULL,
    [LostOpportunityCount]                INT          NULL,
    [CaptiveLoyalReturnToMarketCount]     INT          NULL,
    [CaptiveLoyalCount]                   INT          NULL,
    [CaptiveLeaseReturnToMarketCount]     INT          NULL,
    [CaptiveLeaseCount]                   INT          NULL,
    [CaptiveRetailRTM]                    INT          NULL,
    [CaptiveRetailLoyalCount]             INT          NULL,
    [NonCaptiveRTM]                       INT          NULL,
    [NonCaptiveLoyalCount]                INT          NULL,
    [NonCaptiveLeaseRTMCount]             INT          NULL,
    [NonCaptiveLeaseLoyalCount]           INT          NULL,
    [NonCaptiveRetailRTM]                 INT          NULL,
    [NonCaptiveRetailLoyalCount]          INT          NULL,
    [UnknownRTM]                          INT          NULL,
    [UnknownLoyalCount]                   INT          NULL,
    [UnknownLeaseRTMCount]                INT          NULL,
    [UnknownLeaseLoyalCount]              INT          NULL,
    [UnknownRetailRTM]                    INT          NULL,
    [UnknownRetailLoyalCount]             INT          NULL
);


GO
CREATE CLUSTERED INDEX [IX_tbl_Honda_CPO_Loyalty_COMMON]
    ON [dbo].[tbl_Honda_CPO_Loyalty]([DealerCode] ASC, [DealerName] ASC, [ReportYearAndMonth] ASC, [ReturntoMarket] ASC, [DealerLoyaltyCount] ASC, [OtherDealerCount] ASC, [BrandLoyaltyCount] ASC, [DefectionCompetitiveSegmentCount] ASC, [DefectionNonCompetitiveSegmentCount] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_CPO_Loyalty_AHFC]
    ON [dbo].[tbl_Honda_CPO_Loyalty]([FinanceRegionCode] ASC, [FinanceRegionName] ASC, [FinanceDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_CPO_Loyalty_AHM]
    ON [dbo].[tbl_Honda_CPO_Loyalty]([SalesZoneCode] ASC, [SalesDistrictCode] ASC, [DealerCode] ASC, [ReportYearAndMonth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_CPO_Loyalty_AUTO1]
    ON [dbo].[tbl_Honda_CPO_Loyalty]([DealerCode] ASC, [ReportYearAndMonth] ASC)
    INCLUDE([ReturntoMarket], [DealerLoyaltyCount], [OtherDealerCount], [BrandLoyaltyCount]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_tbl_Honda_CPO_Loyalty_Auto2]
    ON [dbo].[tbl_Honda_CPO_Loyalty]([ReportYearAndMonth] ASC)
    INCLUDE([ReturntoMarket], [DealerLoyaltyCount], [OtherDealerCount], [BrandLoyaltyCount]);


GO
CREATE STATISTICS [_dta_stat_277576027_1_8]
    ON [dbo].[tbl_Honda_CPO_Loyalty]([SalesZoneCode], [ReportYearAndMonth]);


GO
CREATE STATISTICS [_dta_stat_277576027_1_2_8]
    ON [dbo].[tbl_Honda_CPO_Loyalty]([SalesZoneCode], [SalesDistrictCode], [ReportYearAndMonth]);

