﻿EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'ahm_RTM';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'ahm_RTM2';


GO
EXECUTE sp_addrolemember @rolename = N'db_accessadmin', @membername = N'ahm_RTM';


GO
EXECUTE sp_addrolemember @rolename = N'db_accessadmin', @membername = N'ahm_RTM2';


GO
EXECUTE sp_addrolemember @rolename = N'db_securityadmin', @membername = N'ahm_RTM';


GO
EXECUTE sp_addrolemember @rolename = N'db_ddladmin', @membername = N'ahm_RTM';


GO
EXECUTE sp_addrolemember @rolename = N'db_backupoperator', @membername = N'ahm_RTM';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'ahm_RTM';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'ahm_RTM';


GO
EXECUTE sp_addrolemember @rolename = N'db_denydatareader', @membername = N'ahm_RTM';


GO
EXECUTE sp_addrolemember @rolename = N'db_denydatawriter', @membername = N'ahm_RTM';

